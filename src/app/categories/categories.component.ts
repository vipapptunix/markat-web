import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { Options } from 'ng5-slider';
import { JsonPipe, Location } from '@angular/common';
import { from, of } from 'rxjs';
import { max, filter, distinct, distinctUntilChanged } from 'rxjs/operators';
import { TranslateService } from "@ngx-translate/core";
import { A11yModule } from '@angular/cdk/a11y';
import { PlatformLocation } from '@angular/common'
import { MatSnackBar } from '@angular/material';
import { element } from 'protractor';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  index = 0
  id: any = "";
  minprice:any = "0";
  maxprice:any = "5000";
  subCategeory: any = "";
  ratval  = 0;
  currentPage: number = 1;
  search_result: any;
  searchvalue: any;
  search: any;
  brandname: any;
  imgurl: any;
  subproduct: boolean = false;
  searchlist: boolean = false;
  categorylist: any;
  bannerlist: any;
  searchresulthide: boolean = false;
  listsubcategory: any;
  subcategeorylist: any;
  categorycartlist: any;
  categoryid: any;
  productdetail: any;
  fixedrate: any;
  length: any;
  realid: any;
  pagehide: boolean = false;
  realcategory: any;
  isloader:boolean = true;
  row: boolean = true;
  pricehide: boolean = true;
  brandarr: any = [];
  value: number = 0;
  highValue: number = 6000;
  options: Options = {
    floor: 0,
    ceil: 5000,
    enforceStep: false,
    enforceRange: false,
  };
  maxvalue: any;
  isfeature: boolean = true;
  bannerlist_id: any;
  name: any;
  BrandIdArr: any;
  productId:any;
  bannerlist_type: any;
  prod: any;
  namefeat: string;
  bannerlist_list: any;
  brandlist: any = [];
  brandFilter:any = [];
  subcatnumb: any;
  numcheck: number;
  offerlist: any;
  sort = 1;
  catid: any;
  radiocheck: number  = 0;
  checkstatus: any;
  offertype: any;
  number: any;
  brandarrylist: any = [];
  userid: string;
  type: any;
  indexval: number;
  checkout: any;
  sellerpro: boolean = false;
  SeachVal: any;
  isloggin: string;
  column: string = 'rowcat';
  subid: any;
  language: string;
  lang: any;
  brandId: any;
  bannertype: any;
  banneroff: any;
  prodarray: any;
  banenrlist: any;
  subprod: any;
  sortvalue: any = 1;
  page: any = 1;
  count: any = 10;
  result: string;
  currency: any;
  celeb:any;
  constructor(private router: Router,  private snackmsg:MatSnackBar, private route: ActivatedRoute,private translate:TranslateService, public location: Location, public localcheck: PlatformLocation, public authenticationService: AuthenticationService) 
  {
   // this.language = localStorage.getItem('lan')
    this.imgurl = this.authenticationService.imageUrl;
    this.isloggin = localStorage.getItem("sucess");

    
    this.language = localStorage.getItem('lan')
    this.translate.setDefaultLang(this.language);
    this.authenticationService.languagechange.subscribe((res:any)=>
    {
      this.language = res;
    })

    this.authenticationService.$coutChng.subscribe((res:any)=>
  {
    if(res == 'curr')
    {
      this.checkstart();
    }
  })
  }


  ngOnInit() 
  {
    this.language = localStorage.getItem('lan')
    this.banenrlist = JSON.parse(sessionStorage.getItem('bannerlist'))
    this.setRouteval()
    this.getcategoriesList()
  
    this.userid = localStorage.getItem('userid')
    // this.route.queryParams.subscribe(params => { this.ratval = params['rating'] })
    // this.authenticationService. $bannerlist.subscribe((res: any) => {
    //  this.banneroff = res.offer.list
    // })
    this.checkstart()

    this.authenticationService.$searchvalue.subscribe((res:any)=>
    {
      if(res.length != 0)
      {
        this.searchvalue = res
        this.setRouteval()
        setTimeout(() => {
          this.checkstart()
        }, 100);
      }
    })
    
    this.authenticationService.$checkproductid.subscribe((res:any)=>
    {
      if(res.length != 0)
      {
        this.snackmsg.open(this.language == 'en' ? 'Loading....' : 'جار التحميل','',{
          duration: 4000,
        })
       this.brandId = res;
        this.checkstart()
        this.getcategoriesList();
      }
    })
  }

setRouteval()
{
  
  this.language = localStorage.getItem('lan')
  this.route.queryParams.subscribe(params => {
    this.brandId = params['id']
    this.subprod = params['sub']
    this.celeb  = params['celeb']
    this.bannertype = params['type']
    this.searchvalue = params['Sval']
    this.result = params['result']
    this.minprice = params['min'] == undefined ? "0" : params['min']
    this.maxprice = params['max'] == undefined ? "5000" : params['max']
    this.ratval = params['rating'] == undefined ? 0 : params['rating']
    this.radiocheck = this.ratval != 0 ? this.ratval : 0
  });
}

  getexpansevalue(id)
  {
    this.subcategeorylist =   this.categorylist.filter(ele=>ele._id == id)
    this.subcategeorylist = this.subcategeorylist[0].subCats
  }

  subcategoryValue(id)
  {
    this.queryObj['type'] = 'category';
    this.queryObj['sub'] = 1;
    this.queryObj['id']= id;
    this.router.navigate([], { relativeTo: this.route, queryParams: this.queryObj })
    const data=
    {
      "userId":localStorage.getItem('userid'),
      "subCategory":id,
      "page":this.page,
      "count":this.count,
      "sortBy":this.sortvalue,
      "minPrice": this.minprice,
      "maxPrice": this.maxprice,
      "rating": this.radiocheck == 0 ? 0 : this.radiocheck == 1 ? 1 : this.radiocheck == 2 ? 2 : this.radiocheck == 3 ? 3 : this.radiocheck == 4 ? 4 : 5,
      "brands": this.brandarr
    }
    this.authenticationService.getprouctdata(data).subscribe((res:any)=>
    {
      this.isloader = false;
      this.brandname = res.data;
    //  this.brandlist = this.brandname;
      this.length = res.total;
      this.getcategoriesList()
    })
  }

seeallCelebrity()
{
  const data=
  {
    "userId":localStorage.getItem('userid'),
    "page":this.page,
    "count":this.count,
    "sortBy":this.sortvalue,
    "minPrice": this.minprice,
    "maxPrice": this.maxprice,
    "rating": this.radiocheck == 0 ? 0 : this.radiocheck == 1 ? 1 : this.radiocheck == 2 ? 2 : this.radiocheck == 3 ? 3 : this.radiocheck == 4 ? 4 : 5,

    "brands": this.brandarr
  }
  this.authenticationService.sellAllceleb(data).subscribe((res:any)=>
  {
    this.isloader = false;
    this.brandname = res.data;
    this.currency = res.data[0].currency
    //this.brandlist = this.brandname;
    this.length = res.total;
  })
}
  getBrandproduct() 
  {
    const data = 
    {
      "brand": this.brandId,
      "userId":localStorage.getItem('userid'),
      "page":this.page,
      "count":this.count,
      "sortBy":this.sortvalue,
      "minPrice": this.minprice,
      "maxPrice": this.maxprice,
      "rating": this.radiocheck,
      "brands": this.brandarr
    }

    this.authenticationService.getallbrand(data).subscribe((res: any) => {
      this.isloader = false;
      this.brandname = res.data;
      this.length = res.total;
      this.currency = res.data[0].currency
    //  this.brandlist = this.brandname;
      
    })
  }

  topbrandproduct()
  {
    this.sellerpro = true;
    const data =
    {
      "userId":localStorage.getItem('userid'),
      "page":this.page,
      "count":this.count,
      "sortBy":this.sortvalue,
      "minPrice": this.minprice,
      "maxPrice": this.maxprice,
      "rating": this.radiocheck == 0 ? 0 : this.radiocheck == 1 ? 1 : this.radiocheck == 2 ? 2 : this.radiocheck == 3 ? 3 : this.radiocheck == 4 ? 4 : 5,
      "brands": this.brandarr
    }
    this.authenticationService.getToppicks(data).subscribe((res:any)=>
    {
      this.isloader = false;
      this.brandname = res.data;
     
     // this.brandlist = this.brandname;
      this.length = res.total;
      this.currency = res.data[0].currency
    })
  }

  //Get all brand
  checkBrand() {
  this.brandFilter = []
    this.brandname.forEach(element => {
       this.brandFilter.push(element.brand)
    });
   this.brandFilter = this.uniqByKeepLast(this.brandFilter,it => it.name)
     
    // of(this.brandFilter).pipe(distinctUntilChanged((p, q) => p.name === q.name)).subscribe((x:any)=>
    // {
    //   console.log(x);
      
    // })
    this.brandlist = this.brandFilter.filter(it => new RegExp(this.search, "i").test(it.name));
  }

 uniqByKeepLast(a, key) {
    return [
        ...new Map(
            a.map(x => [key(x), x])
        ).values()
    ]
}

  clearBrand() {
    // this.router.navigate([],{relativeTo:this.route,queryParams:{'prod':this.prod,'name':this.name,'id':this.id}})
    this.queryObj['prod'] = this.prod;
    this.queryObj['name'] = this.name;
    this.queryObj['id'] = this.id;
    this.queryObj['catid'] = this.catid
    delete this.queryObj['cat']
    this.router.navigate([], { relativeTo: this.route, queryParams: this.queryObj })
    setTimeout(() => {
      this.clearRating();
    }, 500);
   
  }

  clearRating() {
    this.queryObj['prod'] = this.prod;
    this.queryObj['name'] = this.name;
    this.queryObj['id'] = this.id;
    this.queryObj['catid'] = this.catid
    this.queryObj['cat'] = this.brandarrylist.join(",");
    delete this.queryObj['rating']
    this.router.navigate([], { relativeTo: this.route, queryParams: this.queryObj })
    setTimeout(() => {
      window.location.reload();
    }, 500);
  }


  //get all the feature data
  getbannnerdata() {

   
    const data =
    {
      "list":this.banenrlist.list,
      "type": this.celeb == 'adver' ? 'product' : 'category',
      "userId":localStorage.getItem('userid'),
      "page":this.page,
      "count":this.count,
      "sortBy":this.sortvalue,
      "minPrice": this.minprice,
      "maxPrice": this.maxprice,
      "rating": this.radiocheck == 0 ? 0 : this.radiocheck == 1 ? 1 : this.radiocheck == 2 ? 2 : this.radiocheck == 3 ? 3 : this.radiocheck == 4 ? 4 : 5,
      "brands": this.brandarr
    }
  this.authenticationService.getbannerproduct(data).subscribe((res:any)=>
  {  
    this.isloader = false;
    this.brandname = res.data;
  
  //  this.brandlist = this.brandname;
    this.length = res.total;
    this.currency = res.data[0].currency
  })
  }


  getcategoriesProd()
  {
    if(this.subprod == 1)
    {
      const data=
      {
        "userId":localStorage.getItem('userid'),
        "subCategory":this.brandId,
        "page":this.page,
        "count":this.count,
        "sortBy":this.sortvalue,
        "minPrice": this.minprice,
        "maxPrice": this.maxprice,
        "rating": this.radiocheck == 0 ? 0 : this.radiocheck == 1 ? 1 : this.radiocheck == 2 ? 2 : this.radiocheck == 3 ? 3 : this.radiocheck == 4 ? 4 : 5,
        "brands": this.brandarr
      }
      this.authenticationService.getprouctdata(data).subscribe((res:any)=>
      {
        this.isloader = false;
        this.brandname = res.data;
        
      //  this.brandlist = this.brandname;
        this.length = res.total;
        this.currency = res.data[0].currency
      })
    }
    if(this.subprod == 0)
    {
      const data=
      {
        "userId":localStorage.getItem('userid'),
        "category":this.brandId,
        "page":this.page,
        "count":this.count,
        "sortBy":this.sortvalue,
        "minPrice": this.minprice,
        "maxPrice": this.maxprice,
        "rating": this.radiocheck == 0 ? 0 : this.radiocheck == 1 ? 1 : this.radiocheck == 2 ? 2 : this.radiocheck == 3 ? 3 : this.radiocheck == 4 ? 4 : 5,
        "brands": this.brandarr
      }
      this.authenticationService.getprouctdata(data).subscribe((res:any)=>
      {
        this.isloader = false;
        this.brandname = res.data;
      
       // this.brandlist = this.brandname;
        this.length = res.total;
        this.currency = res.data[0].currency
      })
    }
  }
 
  sortSelected(event)
  {
    this.sortvalue = event.target.value;
    this.checkstart()
  }

  queryObj = {}
  filterBrancd(i, e, index) 
  {  
    // this.brandarrylist.push(i);
    this.queryObj['type'] = this.bannertype;
    this.queryObj['sub'] = this.subprod;
    this.queryObj['id'] = this.brandId;
    //this.queryObj['catid'] = this.catid
    this.queryObj['Sval'] = this.searchvalue
    this.queryObj['result'] = this.result
    this.queryObj['brand'] = this.brandarrylist.join(",");
    this.router.navigate([], { relativeTo: this.route, queryParams: this.queryObj })
    if (e.checked) 
    {
      this.brandarrylist.push(i);
      this.queryObj['type'] = this.bannertype;
      //this.queryObj['name'] = this.name;
      this.queryObj['id'] = this.brandId;
      //this.queryObj['catid'] = this.catid;
      this.queryObj['brand'] = this.brandarrylist.join(",");
      this.router.navigate([], { relativeTo: this.route, queryParams: this.queryObj })
      this.brandarr.push(i)
      this.checkstart()
     
    }
    if (!e.checked) 
    {
      this.brandarrylist = this.brandarrylist.filter(brand => brand != i)
     // this.queryObj['brand'] = this.brandarrylist.join(",");
      this.router.navigate([], { relativeTo: this.route, queryParams: this.queryObj })
      this.indexval = this.brandarr.indexOf(i);
      this.brandarr.splice(this.indexval, 1)
      this.checkstart()
    }
    //  this.router.navigate(['/categories'],{queryParams: {'brand':'checked'}})
  }


  checkstart()
  {
  
    if(this.bannertype == 'brand')
    {
      this.getBrandproduct()
    }
    if(this.bannertype == 'Homebannere')
    {
      this.getbannnerdata();
    } 
    if(this.bannertype == 'expand')
    {
     this.seeallCelebrity()
    }
    if(this.bannertype == 'Topexpand')
    {
      this.topbrandproduct()
    }
    if(this.bannertype == 'category')
    {
      this.getcategoriesProd()
    }
    if(this.result == 'search')
    {
      this.searchResult()
    }
  }

  searchResult()
  {
    const data=
    {
      "searchText":this.searchvalue,
      "userId":localStorage.getItem('userid'),
      "type":'product',
      "page":this.page,
      "count":this.count,
      "sortBy":this.sortvalue,
      "minPrice": this.minprice,
      "maxPrice": this.maxprice,
      "rating": this.radiocheck == 0 ? 0 : this.radiocheck == 1 ? 1 : this.radiocheck == 2 ? 2 : this.radiocheck == 3 ? 3 : this.radiocheck == 4 ? 4 : 5,
      "brands": this.brandarr
    }
    this.authenticationService.getSearchresult(data).subscribe((res:any)=>
    {
      
      this.isloader = false;
    
      this.brandname = res.data.products;
      this.length = res.total;
      this.currency = res.data.products[0].currency
    })
  }

  addfavlist(id)
  {
    if(this.isloggin != null)
    {
      const data =
      {
       "product":id,
        "userId":localStorage.getItem('userid')
      }
      this.snackmsg.open(this.language == 'en' ? 'Loading....': 'جار التحميل',this.language == 'en' ? 'Wishlist' : 'قائمة الرغبات',{
       duration: 8000,
     })
     this.authenticationService.postfavlist(data).subscribe((res:any)=>
     {
       if(res.success)
       {
         this.productId = res.data.product
         this.prodarray = this.brandname.filter(element => element._id == this.productId)
         this.prodarray[0].isFavourite = !this.prodarray[0].isFavourite
         if(this.prodarray[0].isFavourite)
         {
          this.snackmsg.open(this.language == 'en' ? 'Added to wishlist' : 'أضيف لقائمة الأماني',this.language == 'en' ? 'Wishlist' : 'قائمة الرغبات',{
            duration: 3000,
          }).onAction().subscribe((res)=>
          {
            this.router.navigate(['/wishlist'])
          })
         }
         if(!this.prodarray[0].isFavourite)
         {
          this.snackmsg.open(this.language == 'en' ? 'Removed to wishlist' : 'تمت إزالته من قائمة الرغبات',this.language == 'en' ? 'Wishlist' : 'قائمة الرغبات',{
            duration: 3000,
          }).onAction().subscribe((res)=>
          {
            this.router.navigate(['/wishlist'])
          })
         }
    
       }
     });
    }
    else
    {
      this.snackmsg.open(this.language == 'en' ? 'Please login first!' : 'الرجاء تسجيل الدخول أولا!',this.language == 'en' ? 'Login' : 'تسجيل الدخول',
      {
       duration: 4000,
     }).onAction().subscribe((res)=>
      {
        this.router.navigate(['/login'])
      })
    }
  }


  //Price Added
  priceAdd()
  {

    this.queryObj['result'] = this.result
    this.queryObj['type'] = this.bannertype;
    this.queryObj['min'] = this.minprice;
    this.queryObj['max'] = this.maxprice;
    this.queryObj['Sval'] = this.searchvalue
    this.queryObj['id'] = this.brandId;
    this.queryObj['sub'] = this.subprod
    this.router.navigate([],{queryParams:this.queryObj})
    this.snackmsg.open( this.language == 'en' ? 'Loading....' : 'جار التحميل....','Filter',{
      duration:1000
    })
    this.checkstart()

  }

  //rating product
  radioChange(e) 
  {
    // this.queryObj['prod'] = this.prod;
    // this.queryObj['name'] = this.name;
    // this.queryObj['id'] = this.id;
    this.queryObj['result'] = this.result
    this.queryObj['min'] = this.minprice;
    this.queryObj['max'] = this.maxprice;
    this.queryObj['Sval'] = this.searchvalue
    this.queryObj['type'] = this.bannertype;
     this.queryObj['sub'] = this.subprod;
     this.queryObj['id'] = this.brandId;
    // this.queryObj['catid'] = this.catid
    this.ratval = e.value;
    this.queryObj['rating'] = e.value
    this.router.navigate([], { queryParams: this.queryObj })
    this.radiocheck = e.value;
    this.checkstart()
    // this.router.navigate(['/categories'],{queryParams:{'rating':e.value}})
  }


getcategoriesList()
{
  if(this.bannertype == 'Homebannere')
  {
    const data=
    {
      "categories": this.banenrlist.list,
  }
    this.authenticationService.getCategorieslist(data).subscribe((res:any)=>
    {
      this.isloader = false;
     this.categorylist = res.data.categories
     this.brandlist =  res.data.brands
    })
  }
  if(this.bannertype == 'category' && this.subprod == 0)
  {
    const data=
    {
      "categories": [this.brandId],
  }
    this.authenticationService.getCategorieslist(data).subscribe((res:any)=>
    {
      this.isloader = false;
     this.categorylist = res.data.categories
     this.brandlist =  res.data.brands
    })
  }
  if(this.bannertype == 'category' && this.subprod == 1)
  {
    const data=
    {
      "subCategories": [this.brandId],
  }
    this.authenticationService.getCategorieslist(data).subscribe((res:any)=>
    {
      this.isloader = false;
     this.categorylist = res.data.categories
     this.brandlist =  res.data.brands
    })
  }
  if(this.bannertype == 'brand')
  {
    const data=
    {
      "brands": [this.brandId],
  }
    this.authenticationService.getCategorieslist(data).subscribe((res:any)=>
    {
      this.isloader = false;
     this.categorylist = res.data.categories
     this.brandlist =  res.data.brands
    })
  }

}


backtohome()
{
  this.router.navigate(['/home']);
}

  pageeventvalue(e,value) {
   this.page = e.pageIndex + 1;
   this.count = e.pageSize ;
      // this.sliderEvent(e.pageIndex + 1, e.pageSize, 1)
      this.checkstart()
  }

  rowmethod(e) {
    if (e == 1) {
      this.row = false;
      this.column = 'columncat'
    }
    else {
      this.row = true;
      this.column = 'rowcat'
    }
  }

  goTosignup() {
    this.router.navigate(['/signup']);
  }

  goTologin() {
    this.router.navigate(['/login']);
  }
  
  Help()
  {
    this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
  }


  languageChange(lan)
  {
    this.translate.use(lan)
    localStorage.setItem('lan',lan)
    // window.location.reload()
    this.lang = lan
   
    this.authenticationService.languageChange(lan)
   //this.router.navigate([],{queryParams:{lan}})
  }

  gotoproductDetails(id,type) {

      this.router.navigate(['/productdetail'],{queryParams:{'name':'product','id':id}})
  }
  gotoWhatsapp()
  {
    window.open('https://api.whatsapp.com/send?phone=+918288932697')
  }
}
