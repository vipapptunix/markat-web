import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/auth.service';
import { Router } from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-headerpage',
  templateUrl: './headerpage.component.html',
  styleUrls: ['./headerpage.component.scss']
})
export class HeaderpageComponent implements OnInit {

  public  subcategorydataid :any;
  public cate_id:any;
  public subcat_id:any;
  public imgurl:any;
  public productdata:any;
  isloggin: string;
  language: string;
  constructor(private authservice:AuthenticationService,private router:Router,private translate:TranslateService )
  { 
    this.isloggin = localStorage.getItem("sucess");
    this.imgurl=this.authservice.imageUrl;
  }

  ngOnInit() {
    this.authservice.languagechange.subscribe((res:any)=>
    {
      this.language = res
      this.translate.use(this.language) 
    })
  }

  gotoproductDetails(id)
{
  this.router.navigate(['productdetail'], { queryParams: { 'id': id }});
}
goTosignup() 
{
  this.router.navigate(['/signup']);
}

goTologin() 
{
  debugger
  document.getElementById('myModal').click()
  this.router.navigate(['/login']);
}
Help()
{
  this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
}
}
