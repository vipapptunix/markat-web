import {
  Component,
  OnInit,
  AfterViewInit,
  Inject,
  Output,
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationService } from "../services/auth.service";
import { CommonService } from "../services/common.service";
import { NgxSpinnerService } from "ngx-spinner";
import { EventEmitter } from "@angular/core";
import { map, filter, take, switchMap } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SlidesOutputData, OwlOptions } from "ngx-owl-carousel-o";
import { type } from "os";
import { Type } from "@angular/compiler/src/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { LazyLoadScriptService } from "../services/lazy-load-script.service";
export interface DialogData {
  animal: string;
  name: string;
}
declare var $: any;
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit, AfterViewInit {
  Isloader: boolean = true;

  homeData: any;
  activeSlides: SlidesOutputData;
  slidesStore: any[];
  image: any;
  bannerData: any;
  categoryData: any;
  featuredData: any;
  bestsellerData: any;
  offerData: any;
  showPopup: boolean = true;
  changecolor: boolean = false;
  favicon: any;
  moretoexplore: any[] = [];
  recommendedData: any;
  googleid: any;
  checkstatus: any;
  profiledata: any;
  googledata: any;
  bannerDataVal: any[] = [];
  categoryDataVal: any[] = [];
  featuredDataVal: any[] = [];
  bestsellerDataVal: any[] = [];
  bestseller: any[] = [];
  offerDataVal: any[] = [];
  recommendedDataVal: any[] = [];
  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    dots: true,
  };
  isloggin: any;
  imgurl: any;
  mokabookPayStatus: any;
  language: any;
  lang: any;
  homedata: any;
  featuredata: any;
  celebritydata: any;
  branddata: any;
  sellerdata: any;
  offerdata: any;
  purchasedata: any;
  bannerdata: any;
  picksdata: any;
  favmess: any;
  name: any;
  dialogRef: MatDialogRef<DialogbannerComponent, any>;
  animal: any;
  popup: boolean;
  customOptions: {
    rtl: boolean;
    loop: boolean;
    mouseDrag: boolean;
    touchDrag: boolean;
    pullDrag: boolean;
    dots: boolean;
    dotsEach: boolean;
    autoWidth: boolean;
    slideBy: number;
    autoplay: boolean;
    navSpeed: number;
    navText: string[];
    responsive: {};
    nav: boolean;
  };
  categoryoptions: {
    loop: boolean;
    mouseDrag: boolean;
    touchDrag: boolean;
    pullDrag: boolean;
    dots: boolean;
    dotsEach: boolean;
    autoWidth: boolean;
    slideBy: number;
    freeDrag: boolean;
    autoplay: boolean;
    navSpeed: number;
    navText: string[];
    responsive: {};
    nav: boolean;
  };
  customRecommended: {
    rtl: boolean;
    loop: boolean;
    mouseDrag: boolean;
    freeDrag: boolean;
    autoplay: boolean;
    touchDrag: boolean;
    pullDrag: boolean;
    dots: boolean;
    dotsEach: boolean;
    nav: boolean;
    slideBy: number;
    autoWidth: boolean;
    navText: string[];
    responsive: {};
  };
  customRecommended2: {
    rtl: boolean;
    loop: boolean;
    mouseDrag: boolean;
    freeDrag: boolean;
    autoplay: boolean;
    touchDrag: boolean;
    pullDrag: boolean;
    dots: boolean;
    dotsEach: boolean;
    nav: boolean;
    slideBy: number;
    autoWidth: boolean;
    navText: string[];
    responsive: {};
  };
  customdealday: {
    rtl: boolean;
    loop: boolean;
    mouseDrag: boolean;
    freeDrag: boolean;
    autoplay: boolean;
    touchDrag: boolean;
    pullDrag: boolean;
    dots: boolean;
    dotsEach: boolean;
    nav: boolean;
    slideBy: number;
    autoWidth: boolean;
    navText: string[];
    responsive: {};
  };
  customBrand: {
    rtl: boolean;
    loop: boolean;
    mouseDrag: boolean;
    freeDrag: boolean;
    autoplay: boolean;
    touchDrag: boolean;
    pullDrag: boolean;
    dots: boolean;
    dotsEach: boolean;
    nav: boolean;
    slideBy: number;
    autoWidth: boolean;
    navText: string[];
    responsive: {};
  };
  home: any;
  see_all: any;
  celebrity_endorsed: any;
  top_sellers: any;

  constructor(
    private router: Router,
    public authenticationService: AuthenticationService,
    private commonService: CommonService,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private snackmsg: MatSnackBar,
    private lazyLoadService: LazyLoadScriptService,
    public dialog: MatDialog,
    private _route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {
    this.popup =
      sessionStorage.getItem("pop") == null
        ? true
        : JSON.parse(sessionStorage.getItem("pop"));
    console.log("popup", sessionStorage.getItem("pop"), this.popup);
    this.isloggin = localStorage.getItem("sucess");
    this.googledata = localStorage.getItem("googledata");

    if (localStorage.getItem("sucess") && this.googleid == null) {
      this.authenticationService.getprofile().subscribe((profdata: any) => {
        this.profiledata = profdata.data._id;
        localStorage.setItem("userid", this.profiledata);
      });
    }
    this.authenticationService.languagechange.subscribe((res: any) => {
      this.language = res;
    });
    this.authenticationService.gettranslation().subscribe((res: any) => {
    });

  }

  ngOnInit() {
    this.language = localStorage.getItem("lan");
    this.imgurl = this.authenticationService.imageUrl;
    this.favicon = "assets/images/like.png";
    this.featuredata = [];
    this.getHomedata();
    this.route.queryParams.subscribe((params) => {
      this.checkstatus = params["gatewaySays"];
      this.mokabookPayStatus = params["mokabookPayStatus"];
    });
    this.authenticationService.$coutChng.subscribe((res: any) => {
      if (res == "curr") {
        this.getHomeagain();
      }
    });

    this.customOptions = {
      rtl: this.language == "arb" ? true : false,
      loop: true,
      mouseDrag: true,
      touchDrag: true,
      pullDrag: true,
      dots: false,
      dotsEach: true,
      autoWidth: false,
      slideBy: 1,
      autoplay: true,
      navSpeed: 70,
      navText: ["", ""],
      responsive: {
        0: {
          items: 1,
        },
        400: {
          items: 1,
        },
        740: {
          items: 1,
        },
        940: {
          items: 1,
        },
      },
      nav: false,
    };

    this.categoryoptions = {
      loop: false,
      mouseDrag: true,
      touchDrag: true,
      pullDrag: false,
      dots: false,
      dotsEach: false,
      autoWidth: false,
      slideBy: 1,
      freeDrag: false,
      autoplay: false,
      navSpeed: 70,
      navText: ["", ""],
      responsive: {
        0: {
          items: 3,
        },
        400: {
          items: 8,
        },
        740: {
          items: 8,
        },
        940: {
          items: 8,
        },
      },
      nav: false,
    };

    this.customRecommended = {
      rtl: this.language == "arb" ? true : false,
      loop: false,
      mouseDrag: true,
      freeDrag: false,
      autoplay: false,
      touchDrag: true,
      pullDrag: true,
      dots: false,
      dotsEach: false,
      nav: true,
      slideBy: 1,
      autoWidth: false,
      navText: ["<<", ">>"],
      responsive: {
        0: {
          items: 2,
        },
        400: {
          items: 4,
        },
        740: {
          items: 4,
        },
        940: {
          items: 4,
        },
      },
    };

    this.customRecommended2 = {
      rtl: this.language == "arb" ? true : false,
      loop: false,
      mouseDrag: true,
      freeDrag: true,
      autoplay: false,
      touchDrag: true,
      pullDrag: true,
      dots: false,
      dotsEach: false,
      nav: true,
      slideBy: 1,
      autoWidth: false,
      navText: ["<<", ">>"],
      responsive: {
        0: {
          items: 2,
        },
        400: {
          items: 4,
        },
        740: {
          items: 4,
        },
        940: {
          items: 5,
        },
      },
    };

    this.customdealday = {
      rtl: this.language == "arb" ? true : false,
      loop: false,
      mouseDrag: false,
      freeDrag: true,
      autoplay: false,
      touchDrag: false,
      pullDrag: false,
      dots: false,
      dotsEach: false,
      nav: true,
      slideBy: 1,
      autoWidth: false,
      navText: ["<<", ">>"],
      responsive: {
        0: {
          items: 2,
        },
        400: {
          items: 4,
        },
        740: {
          items: 4,
        },
        940: {
          items: 5,
        },
      },
    };

    this.customBrand = {
      rtl: this.language == "arb" ? true : false,
      loop: false,
      mouseDrag: true,
      freeDrag: true,
      autoplay: false,
      touchDrag: true,
      pullDrag: true,
      dots: false,
      dotsEach: false,
      nav: true,
      slideBy: 1,
      autoWidth: false,
      navText: ["<<", ">>"],
      responsive: {
        0: {
          items: 2,
        },
        400: {
          items: 3,
        },
        740: {
          items: 3,
        },
        940: {
          items: 3,
        },
      },
    };
  }

  slickSlider() {
    console.log("afterviewcontent", this.branddata);
    this.lazyLoadService
      .loadScript(
        "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
      )
      .pipe(
        map((_) => "jQuery is loaded"),
        filter((jquery) => !!jquery),
        take(1),
        switchMap((_) =>
          this.lazyLoadService.loadScript(
            "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"
          )
        )
      )
      .subscribe((_) => {
        $(".slick-container").slick({
          dots: false,
          infinite: true,
          rtl: this.language == "arb" ? true : false,
          speed: 300,
          rows: 2,
          slidesToShow: 5,
          slidesToScroll: 4,
          cevterPaddiing: "20px",
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: false,
              },
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              },
            },
          ],
        });
      });
  }

  gotoproductDetails(id) {
    this.router.navigate(["productdetail"], { queryParams: { id: id } });
  }

  getHomedata() {
    var id = localStorage.getItem("userid");

    this.authenticationService.getDashboard().subscribe((res: any) => {
      console.log("dtata", res);
      if (res.data.output[0].label == "Home Banner") {
        this.homedata = res.data.output[0].value;
      }
      if (res.data.output[1].label == "Featured Celebrities") {
        this.featuredata = res.data.output[1].value;
        console.log("this.feature", this.featuredata);
      }
      if (res.data.output[5].label == "Celebrity Endorsed") {
        this.celebritydata = res.data.output[5].value;
      }
      if (res.data.output[4].label == "Top Picks") {
        this.picksdata = res.data.output[4].value;
      }
      if (res.data.output[7].label == "Brands") {
        this.Isloader = false;
        this.branddata = res.data.output[7].value;
      }
      if (res.data.output[6].label == "Top Sellers") {
        this.sellerdata = res.data.output[6].value;
      }
      if (res.data.output[8].label == "Best Offers") {
        this.Isloader = false;
        this.offerdata = res.data.output[8].value;
      }
      if (res.data.output[2].label == "Purchase By Category") {
        this.purchasedata = res.data.output[2].value;
      }
      if (res.data.output[3].label == "Banner") {
        this.slickSlider();
        this.Isloader = false;
        this.bannerdata = res.data.output[3].value;
      }
      if (res.data.output[9].label == "Popup Banner") {
        if (this.popup) {
          this.dialogRef = this.dialog.open(DialogbannerComponent, {
            width: "250px",
            data: { name: this.name, animal: this.animal },
          });
        }
      }
    });
  }

  ngAfterViewInit() {
    this.authenticationService.getDashboard().subscribe((res: any) => {
      if (res.data.output[1].label == "Featured Celebrities") {
        this.featuredata = res.data.output[1].value;
      }
    });

    this.authenticationService.gettranslation().subscribe((res: any) => {
      this.home = res.data.data["home"];
      this.see_all = res.data.data["see_all"];
      this.celebrity_endorsed = res.data.data["celebrity_endorsed"];
      this.top_sellers = res.data.data["top_sellers"];
    });
  }

  getHomeagain() {
    var id = localStorage.getItem("userid");

    this.authenticationService.getDashboard().subscribe((res: any) => {
      console.log("dtata", res);
      if (res.data.output[0].label == "Home Banner") {
        this.homedata = res.data.output[0].value;
      }
      if (res.data.output[5].label == "Celebrity Endorsed") {
        this.celebritydata = res.data.output[5].value;
      }
      if (res.data.output[4].label == "Top Picks") {
        this.picksdata = res.data.output[4].value;
      }
      if (res.data.output[7].label == "Brands") {
        this.Isloader = false;
        this.branddata = res.data.output[7].value;
        this.slickSlider();
      }
      if (res.data.output[8].label == "Best Offers") {
        this.Isloader = false;
        this.offerdata = res.data.output[8].value;
      }
      if (res.data.output[2].label == "Purchase By Category") {
        this.purchasedata = res.data.output[2].value;
      }
      if (res.data.output[3].label == "Banner") {
        this.Isloader = false;
        this.bannerdata = res.data.output[3].value;
      }
    });
  }

  closedialog(data) {
    this.dialogRef.afterClosed().subscribe((result) => {
      this.router.navigate(["/home"]);
      this.animal = result;
    });
  }
  seeAllcelebrity() {
    this.router.navigate(["/categories"], { queryParams: { type: "expand" } });
  }

  checktopproduct() {
    this.router.navigate(["/categories"], {
      queryParams: { type: "Topexpand" },
    });
  }

  goTocategoriesdetail(id) {
    this.router.navigate(["/categoriesdetail"], { queryParams: { id: id } });
  }
  goTocategories(value, name, data) {
    this.router.navigate(["/categories"], {
      queryParams: { prod: name, name: "moretoexplore", id: value },
    });
    // this.router.navigate(['/categories']);
  }

  goTosignup() {
    this.router.navigate(["/signup"]);
  }

  goTologin() {
    this.router.navigate(["/login"]);
  }

  goTocelebrityindex() {
    this.router.navigate(["/celebrityindex"], {
      queryParams: { feature: "" },
    });
  }
  goTosellerindex() {
    this.router.navigate(["/sellerindex"]);
  }
  goTobrandindex() {
    this.router.navigate(["/brandindex"]);
  }
  goTocelebrityprofile(item) {
    this.router.navigate(["/celebrityprofile"], {
      queryParams: { id: item._id },
    });
  }
  goTosellerprofile() {
    this.router.navigate(["/sellerprofile"]);
  }

  addfavlist(id) {
    if (this.isloggin != null) {
      const data = {
        product: id,
        userId: localStorage.getItem("userid"),
      };
      this.snackmsg.open("Loading....", "Wishlist", {
        duration: 8000,
      });
      this.authenticationService.postfavlist(data).subscribe((res: any) => {
        if (res.success) {
          this.favmess = res.message;
          this.changecolor = true;
          this.getfavlistdata();
        }
      });
    } else {
      this.snackmsg
        .open("Please login first!", "Login", {
          duration: 4000,
        })
        .onAction()
        .subscribe((res) => {
          this.router.navigate(["/login"]);
        });
    }
  }

  gotforbrand(id) {
    this.router.navigate(["/categories"], {
      queryParams: { id: id, type: "brand" },
    });
  }

  getfavlistdata() {
    var id = localStorage.getItem("userid");

    this.authenticationService.getDashboard().subscribe((res: any) => {
      this.snackmsg.dismiss();
      this.snackmsg
        .open(this.favmess, "Wishlist", {
          duration: 4000,
        })
        .onAction()
        .subscribe((res) => {
          this.router.navigate(["/wishlist"]);
        });
      if (res.data.output[5].label == "Celebrity Endorsed") {
        this.celebritydata = res.data.output[5].value;
      }
      if (res.data.output[4].label == "Top Picks") {
        this.picksdata = res.data.output[4].value;
      }
    });
  }

  bannerpage(data) {
    data.type = "bannerlist";
    //data.push(obj);
    this.authenticationService.getbannerdata(data);
    sessionStorage.setItem("bannerlist", JSON.stringify(data.offer));
    this.router.navigate(["/categories"], {
      queryParams: { type: "Homebannere", id: data._id },
    });
  }

  featurePage(data) {
    data.type = "Feature";
    this.authenticationService.getbannerdata(data);
    this.router.navigate(["/categories"], {
      queryParams: { prod: "Feature", id: data._id },
    });
  }

  Help() {
    this.router.navigate(["/terms&cond"], { queryParams: { name: "Help" } });
  }

  languageChange(lan) {
    this.translate.use(lan);
    localStorage.setItem("lan", lan);
    // window.location.reload()
    this.lang = lan;
    if (this.lang == "en")
      if (this.lang == "arb") this.authenticationService.languageChange(lan);
    //this.router.navigate([],{queryParams:{lan}})
  }
  gotoWhatsapp() {
    window.open("https://api.whatsapp.com/send?phone=+918288932697");
  }
}

@Component({
  selector: "banner-example-dialog",
  templateUrl: "bannerpop.component.html",
})
export class DialogbannerComponent {
  imgurl: string;
  popupbanner: any;
  @Output() newItemEvent = new EventEmitter<string>();
  constructor(
    public dialogRef: MatDialogRef<DialogbannerComponent>,
    private router: Router,
    public authenticationService: AuthenticationService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.imgurl = this.authenticationService.imageUrl;
    this.authenticationService.getDashboard().subscribe((res: any) => {
      if (res.data.output[9].label == "Popup Banner") {
        this.popupbanner = res.data.output[9].value[0];
      }
    });
  }

  onNoClick(data): void {
    sessionStorage.setItem("pop", "false");
    this.dialogRef.close();
    data.type = "bannerlist";
    this.authenticationService.getbannerdata(data);
    sessionStorage.setItem("bannerlist", JSON.stringify(data.offer));
    this.router.navigate(["/categories"], {
      queryParams: { type: "Homebannere", id: data._id },
    });
  }

  addNewItem(value) {
    console.log(value);
    this.newItemEvent.emit(value);
  }

  onclose() {
    sessionStorage.setItem("pop", "false");
    this.dialogRef.close();
  }
}
