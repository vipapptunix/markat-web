import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as io from 'socket.io-client';
import { CommonService } from './common.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
// import { Observable } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/internal/operators';
@Injectable({
    providedIn: 'root'
})
export class NotificationService {
    socket;
    constructor(private authSrv: AuthenticationService){this.setupSocketConnection()}
    
    setupSocketConnection() {
        this.socket = io(this.authSrv.imageUrl);
        console.log("socket connected");
    }


   checkListen()
   {
    var userid =localStorage.getItem('userid')
    return new Observable (observer => {
        this.socket.removeListener(`payment-${userid}`);
        this.socket.on(`payment-${userid}`, message => {
          observer.next(message);
        });
      });
   }

}