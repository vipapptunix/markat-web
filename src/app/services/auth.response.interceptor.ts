import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { CommonService } from "./common.service";
import { Router } from "@angular/router";

@Injectable({ providedIn: "root" })
export class AuthResponseInterceptor implements HttpInterceptor {
  token = "";
  gender:string;
  viewIs;
  language: string;
  constructor(private commonService: CommonService, private router: Router) {
    router.events.subscribe((val) => {

      this.viewIs = window.location.href;
      var lastSlash = window.location.href.lastIndexOf('/home');
      this.viewIs = this.viewIs.substr(lastSlash + 1);

    });
     
  }
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> { 
    this.language = localStorage.getItem('lan') == null ? 'arb' : localStorage.getItem('lan')
    const token: string = localStorage.getItem("confirmauth");
    const gender:string = localStorage.getItem('gender')
    const latitude = sessionStorage.getItem('latitude') == null ? '30.7046486' : sessionStorage.getItem('latitude'); 
    const longitude = sessionStorage.getItem('longitude') == null ? '76.71787259999999' : sessionStorage.getItem('longitude'); 
    const country = localStorage.getItem('countryCode') == null ? 'IN' : localStorage.getItem('countryCode');
/*
sessionStorage.setItem('latitude','30.7046486');
sessionStorage.setItem('longitude','76.71787259999999')
*/
    if (token) {
      req = req.clone({ headers: req.headers.set("Authorization", token) });
    }
    req = req.clone({headers:req.headers.set("lang",this.language)})
    req = req.clone({headers:req.headers.set("gender",gender)})
    req = req.clone({headers:req.headers.set("latitude",latitude)})
    req = req.clone({headers:req.headers.set("longitude",longitude)})
    req = req.clone({headers:req.headers.set("countryshortcode",country)})
    req = req.clone({ headers: req.headers.set("Accept", "application/json") });
    
    return next.handle(req).pipe(catchError(err => this.handleError(err)));
  }

  private handleError(err: HttpErrorResponse): Observable<any> {

    let errorMsg;
    if (err.error instanceof Error) {

      //errorMsg = `Error: Unable to connect`;
    } else {
      //errorMsg = `Error: ${err.status}, Unable to connect`;
    }

    if (err.error && (err.status == 401)) {
     // this.commonService.showToasterError('Un-authorized ! Please login first.')

     // this.router.navigate(["/home"]);

   //   this.clearStorage();

    }

    else if (err.status == 400) {
      console.log("Eroorrrrr handler 400" + JSON.stringify(err));
      this.commonService.showToasterError(err.error.message);
      // this.clearStorage();
      // if(this.viewIs=="home"){
      //   this.router.navigate(["/"]);
      // }
      // else{
      //   this.router.navigate(["/home"]);
      // }
    }


    else if (err.status == 403) {
      this.commonService.showToasterError('Please sign-in Again.')
      this.clearStorage();
      if (this.viewIs == "home") {
       // this.router.navigate(["/"]);
      }
      else {
        //this.router.navigate(["/home"]);
      }
    } else if (err.error && err.status == 500 && !err.error.success) 
    {
     // this.commonService.showToasterError('Something is wrong !');
    } else if (err.error && !err.error.success) 
    {
      // this.commonService.showToasterError('Unable to connect, please wait and try to sign-in again.');
     // this.clearStorage();
      if (this.viewIs == "home") {
        //this.router.navigate(["/"]);
      }
      else {
        //this.router.navigate(["/home"]);
      }
      // this.router.navigate(['/']);
    } else {
      if (errorMsg)
        this.commonService.showToasterError(errorMsg)
    }
    return errorMsg;
  }

  clearStorage() {
    localStorage.removeItem("loggedInUser");
   // localStorage.removeItem("confirmauth");
    localStorage.removeItem("verify");
    localStorage.removeItem("changePassword");
    localStorage.removeItem('addresscooldash')
    localStorage.removeItem('ItemsAdded');
    localStorage.removeItem('lattcooldash');
    localStorage.removeItem('longgcooldash');
    localStorage.removeItem('socialuser');
    localStorage.removeItem('fblogin');
    localStorage.removeItem('authTokenn');
    localStorage.removeItem('signUpSocial');
    localStorage.removeItem('trackorder');
    localStorage.removeItem('trackstatus');

  }
}
