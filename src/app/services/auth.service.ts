import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from './common.service';
import { NgxSpinnerService } from "ngx-spinner";
// import { VerificationModalAuthComponent } from '../verification-modal-auth/verification-modal-auth.component';
// import { PhoneConfirmComponent } from '../phone-confirm/phone-confirm.component';
// import { VehicleModalComponent } from '../vehicle-modal/vehicle-modal.component';
// import { VehicleUpdateModalComponent } from '../vehicle-update-modal/vehicle-update-modal.component';
// import { VerificationModalAuthComponent } from '../auth/verification-modal-auth/verification-modal-auth.component';
// import { OutletModalComponent } from '../user/outlet-modal/outlet-modal.component';


@Injectable({
  providedIn: "root"
})
export class AuthenticationService {
  // baseUrl: any = environment.user;
  // baseUrl: string = "https://appgrowthcompany.com:3000/v1/user/";
  baseUrl: string = "https://appgrowthcompany.com:9079/api/app/";
  // http://192.168.1.98:3000/
  // imageUrl:string="https://appgrowthcompany.com:4003";
  imageUrl: string = "https://appgrowthcompany.com:9079/";
  // baseUrlFood:string="https://appgrowthcompany.com:3000/v1/web/";
  // imageUrlFood:string="https://appgrowthcompany.com:3000";
  countryCode: any;
  authorization: any;
  // adminUrl:string="https://appgrowthcompany.com:3000/v1/admin/food/";
  public Userdata = new BehaviorSubject<any>([]);
  private loggedIn = new BehaviorSubject<boolean>(false);

  public hist_id = new BehaviorSubject<any>('');
  public homeprofile = new BehaviorSubject<any>('');

  public homeprofname = this.homeprofile.asObservable();
  public historyid = this.hist_id.asObservable();

  public googlefbdata = new BehaviorSubject<any>([]);
  public googlefbget = this.googlefbdata.asObservable();

  public search_value = new BehaviorSubject<any>([]);
  public $searchvalue = this.search_value.asObservable();

  public cartlen = new BehaviorSubject<any>('');
  public cart_length = this.cartlen.asObservable();

  public checkproduct = new BehaviorSubject<any>([]);
  public $checkproductid = this.checkproduct.asObservable();

  public termscond = new BehaviorSubject<any>('');
  public languagechange = new BehaviorSubject<any>('');

  public $termscond = this.termscond.asObservable();

  public propic_header = new BehaviorSubject<any>([]);
  public $profilepic_check = this.propic_header.asObservable();

  public footer = new BehaviorSubject<any>('');
  public $footer = this.footer.asObservable();

  public updateprod = new BehaviorSubject<any>('');
  public $updatepoduct = this.updateprod.asObservable();

  guestprod: any[] = [];

  public bannerdata = new BehaviorSubject<any>([]);
  public $bannerlist = this.bannerdata.asObservable();

  public signup = new BehaviorSubject<any>('');
  public $chooseoption = this.signup.asObservable();

  public countrychng = new BehaviorSubject<any>('');
  public $coutChng = this.countrychng.asObservable();

  usercred: any;
  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  constructor(
    private http: HttpClient,
    public dialog: MatDialog,
    private router: Router,
    private commonService: CommonService,
    private _route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {
    this.getCountryCode();
    this.authorization = localStorage.getItem('confirmauth');

  }

  getCountryCode() {
    this.http.get("assets/json/countryCode.json").subscribe(data => {
      this.countryCode = data;
    })
  }

  categoryProducts(data) {
    return this.http.get<any>(this.baseUrl + `categoryProducts?category=${data.cat}&userId=${data.userid}`);
  }

  subCategoryProducts(data) {

    return this.http.get<any>(this.baseUrl + 'subCategoryProducts?subCategory=' + data);
  }

  completeRegistration(obj) {
    return this.http.post<any>(`${this.baseUrl}signup`, obj);
  }

  sendVerificationLink(data) {

    return this.http.post<any>(`${this.baseUrl}sendVerificationLink`, data);
  }

  resendVerification(data) {
    return this.http.post<any>(`${this.baseUrl}resendVerification`, data);
  }


  forgetpassword(data) {
    return this.http.post<any>(`${this.baseUrl}forgotPassword`, data);
  }


  languageChange(value) {
    this.languagechange.next(value);
  }

  cancelorder(dat, id) {

    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': this.authorization })
    }
    return this.http.post<any>(`${this.baseUrl}cancelOrder/${id}`, dat, httpOptions);
  }

  getfooterProd() {
    this.footer.next('footer');
  }

  transfersign(data) {
    this.signup.next(data)
  }


  productDetail(data) 
  {
    return this.http.post<any>(this.baseUrl + 'productDetail', data);
  }

  removecarddata(id) {

    // const _id = new HttpParams()
    //    _id.append('id',id)
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'authorization': authorization })
    }
    return this.http.delete<any>(`${this.baseUrl}cart/${id}`, httpOptions);
    // return this.http.put<any>(this.baseUrl +'removeCart?id='+id,httpOptions) ;
  }
  ////get
  favoriteproduct() {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'authorization': authorization })
    }

    return this.http.get<any>(`${this.baseUrl}getFavorites?type=1`, httpOptions);
  }
  ////post
  postfavlist(data) {
    // const _id = new HttpParams().set('product',id);

    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }

    return this.http.post<any>(`${this.baseUrl}favorites`, data, httpOptions)
  }

  savelater(data) {
    return this.http.post(`${this.baseUrl}favorites`, data)
  }

  getbannersubcat() {
    //return this.http.get<any>(`${this.imageUrl}api/admin/categories`)
  }

  getprofile() {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'authorization': authorization })
    }
    return this.http.get<any>(`${this.baseUrl}getProfile`, httpOptions);
  }

  changedata(value) {
    const formdata = new FormData();
    formdata.append('profilePic', value.profilePic)
    formdata.append('phone', value.phone.number)
    formdata.append('countryCode', '+91')
    formdata.append('firstName', value.firstName)
    formdata.append('lastName', value.lastName)
    formdata.append('email', value.email)

    const authorization = localStorage.getItem('confirmauth');
    const httpOptions =
    {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }

    return this.http.put<any>(`${this.baseUrl}updateProfile`, formdata, httpOptions);
  }

  authCheck(data) {
    return this.http.post<any>(`${this.baseUrl}checkVerification`, data);
  }

  getUpdatedsocial() {
    return this.http.get<any>(`https://appgrowthcompany.com:9079/api/admin/settings`)
  }

  updateproduct(id) {
    this.updateprod.next(id)
  }


  verifyemail(email) {
    const data = {
      "email": email
    }
    return this.http.post<any>(`${this.baseUrl}sendVerificationLink`, data);

  }

  reSendotp(data) {
    return this.http.post<any>(`${this.baseUrl}resendVerification`, data);
  }

  tranferhistoryid(id) {
    this.hist_id.next(id);
  }

  setaccountname(firstname) {
    this.homeprofile.next(firstname);
  }

  getgooglefbdata(infodata) {
    this.googlefbdata.next(infodata);
  }

  searchdata(text) {
    this.search_value.next(text);
  }

  getcartlength(lenth) {
    this.cartlen.next(lenth);
  }
  sendprofilepic(propic) {
    this.propic_header.next(propic);
  }

  checkproductId(data) {
    this.checkproduct.next(data);
  }

  countryChange(value) {
    this.countrychng.next(value)
  }

  verifyPhone(data) {
    return this.http.post(`${this.baseUrl}verifyPhone`, data)
  }

  gettermsvalue(id) {
    this.termscond.next(id);
  }
  getbannerdata(data) {
    this.bannerdata.next(data);
  }

  login(form) {
    this.spinner.show();
    const baseUrl = this.baseUrl;
    const httpOptions = {
      headers: new HttpHeaders({ 'lang': 'en', 'country': 'india' })
    }
    if (form.email !== '' && form.password !== '') {
      this.loggedIn.next(true);
      this.http.post(baseUrl + `signin`, form)
        .subscribe(
          (data: any) => {
            console.log("inside login data", data);
            this.spinner.hide();
            if (data.success == true) {
              this.spinner.hide();
              localStorage.setItem('userCred', data.data._id)
              localStorage.setItem("confirmauth", data.data.accessToken);
              localStorage.setItem("sucess", data.success);
              sessionStorage.setItem('x-auth', "1")
              this.guestprod = JSON.parse(localStorage.getItem('cartcount'))
              if (this.guestprod) {
                this.spinner.hide();
                this.guestprod.forEach(element => {
                  element.amount = element.amount * element.quantity
                });
                const datacheck = {
                  "data": this.guestprod
                }
                this.postcartlist(datacheck).subscribe((res: any) => {
                 
                })

              }
              this.spinner.hide();
              setTimeout(() => {
                this.router.navigate(['/home']);
              }, 1000);
            } else {
              this.spinner.hide();
              //this.commonService.showToasterError(data.message);
            }

          }
        )
    }
  }

  // getPosition(): Promise<any>
  // {
  //   return new Promise((resolve, reject) => {

  //     navigator.geolocation.getCurrentPosition(resp => {
  //         resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
  //       },
  //       err => {
  //         reject(err);
  //       });
  //   });

  // }

  liveCOuntryCode(data)
  {
    return this.http.post(`${this.baseUrl}getCountryCode`,data);
  }

  getprouctdata(value) {
    return this.http.post<any>(`${this.baseUrl}getProducts`, value);
  }

  //terms&condition
  gettermsandpolicy() 
  {
    return this.http.get<any>('https://appgrowthcompany.com:9079/api/admin/settings')
  }

  //translation-data
  gettranslation()
  {
    return this.http.get(`${this.imageUrl}api/admin/translation`)
  }

  //getReciept
  printReciept(id) {
    return this.http.get<any>(`${this.baseUrl}receiptPDF?orderid=${id}`)
  }

  contactUs(data) {
    return this.http.post(`${this.baseUrl}contactUs`, data)
  }

  //getcart
  getcartlist() {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }
    return this.http.get<any>(`${this.baseUrl}cart`, httpOptions)
  }

  Viewcartlist() {
    // const authorization =localStorage.getItem('confirmauth');
    // const httpOptions = {
    //  headers: new HttpHeaders({ 'Authorization':authorization })
    // }
    return this.http.post<any>(`${this.baseUrl}viewCart`, '')
  }

  getPromo()
  {
    return this.http.get<any>(`https://appgrowthcompany.com:9079/api/app/promocode?page=1&count=10`)
  }


  getAllcountry() {
    return this.http.get<any>(`http://dev.webdevelopmentsolution.net:9091/api/v1/vendor/countries`)
  }

  SearchResult() {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }
    return this.http.post(`${this.baseUrl}searchHistory`,'')
  }

  getCompareprod(data) {
    return this.http.post<any>(`${this.baseUrl}compareProducts`, data)
  }

  promoCode(data) {
    return this.http.post<any>(`${this.baseUrl}viewCart`, data)
  }

  getallCountry(data) {
    return this.http.get<any>(`https://api.nunustores.com/api/app/getCountries?page=1&count=6&search=${data.search}`)
  }

  checkdeliveryloc(data) {
    return this.http.post(`${this.baseUrl}checkDelivery`, data)
  }

  //postcart
  postcartlist(list) {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }
    return this.http.post<any>(`${this.baseUrl}cart`, list, httpOptions)
  }

  //getAllbrand
  getallbrand(data) {
    return this.http.post<any>(`${this.baseUrl}getProducts`, data)
  }

  //updatecart
  updatelist(value, i, j) {
    console.log("auh", value)
    const id = value._id
    const payload = new HttpParams()
      .set('quantity', i)

    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }
    return this.http.put<any>(`${this.baseUrl}cart/${id}`, payload, httpOptions)
  }


  getfavdashboard(id) {
    var pageid: number = 1;
    const data =
    {
      "page": pageid,
      "userId": id
    }
    //return this.http.post<any>(`${this.baseUrl}dashboard`,data)
  }

  getDashboard() {
    var pageid: number = 1;
    const data =
    {
      "page": pageid,
      "userId": localStorage.getItem('userCred')
    }
    return this.http.post<any>(this.baseUrl + 'dashboard', data);
  }

  getCategorieslist(data) {
    return this.http.post(`${this.baseUrl}filters`, data)
  }

  changePassword(data) {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'authorization': authorization })
    }
    return this.http.put<any>(`${this.baseUrl}changePassword`, data, httpOptions);
  }

  sellAllceleb(data) {
    return this.http.post<any>(`${this.baseUrl}viewAllCelebrityEndorsed`, data)
  }

  topseeAllprod() {
    const data = {}
    return this.http.post<any>(`${this.baseUrl}viewAllTopSellers`, data)
  }

  getToppicks(data) {
    return this.http.post<any>(`${this.baseUrl}viewAllTopPicks`, data)
  }
  sociallogin(socialdata) {
    const data = {
      "firstName": socialdata.name.split(" ")[0],
      "lastName": socialdata.name.split(" ")[1],
      "provider": socialdata.provider,
      "providerId": socialdata.id,
      "deviceType": "Web application",
      "pushToken": socialdata.token,
      "email": socialdata.email
    }
    console.log("socialdata", data);
    this.http.post<any>(`${this.baseUrl}socialLogin`, data).subscribe((res: any) => {
      //console.log("sucess",res)
      console.log("sucessGoogle", res.data.accessToken)
      //localStorage.setItem("confirmauth",res.data.accessToken)
      localStorage.setItem("sucess", res.success);
      this.Userdata.next(res);
      console.log("sucess", this.Userdata)

      if (res.success) {
        localStorage.setItem("confirmauth", res.data.accessToken)
        this.router.navigate(['/home']);
      }
    });

    //return data;
  }

  getbannerproduct(data) {
    return this.http.post<any>(`${this.baseUrl}getBannerProducts`, data);
  }

  getbannerprolist(data) {
    return this.http.post<any>(`${this.baseUrl}getBannerProducts`, data);
  }

  getBrandtab(data) {
    return this.http.post<any>(`${this.baseUrl}brandsTab`, data)
  }

  getaddress() {

    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'authorization': authorization })
    }

    return this.http.get(`${this.baseUrl}address`, httpOptions);
  }

  sellerCelibrity(data) {
    return this.http.post(`${this.baseUrl}viewAllTopSellers`, data)
  }

  celebrityData(data) {
    return this.http.post<any>(`${this.baseUrl}celebrityTab`, data)
  }

  addaddress(data) {
    //   
    //   console.log("datat",data)
    // const payload = new HttpParams()
    // .set('apartment', data.apartment)
    // .set('buildingName', data.buildingName)
    // .set('state', 'punjab')
    // .set('city',data.city)
    // .set('postalCode',data.postalCode)
    // .set('name',data.name)
    // .set('phone', String(data.phone.number))
    // .set('addressType','Home')
    // .set('country',data.country)
    // .set('countryCode','+91')
    // .set('isDefault','false')

    const dataadd =
    {
      'apartment': data.apartment,
      'buildingName': data.buildingName,
      'state': 'punjab',
      'city': data.city,
      'postalCode': data.postalCode,
      'name': data.name,
      'phone': String(data.phone.number),
      'addressType': 'Home',
      'country': data.country,
      'countryCode': '+91',
      'isDefault': 'false'
    }
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }

    return this.http.post<any>(`${this.baseUrl}address`, dataadd, httpOptions)
  }

  deleteaddress(id) {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'authorization': authorization })
    }
    return this.http.delete<any>(this.baseUrl + 'address?id=' + id, httpOptions)
  }

  editaddress(value) {
    const payload = new HttpParams()
      .set('apartment', value.apartment)
      .set('buildingName', value.buildingName)
      .set('state', value.state)
      .set('city', value.city)
      .set('postalCode', value.postalCode)
      .set('name', value.name)
      .set('phone', value.phone.number)
      .set('addressType', value.addressType)
      .set('country', value.country)
      .set('countryCode', '+91')
      .set('isDefault', 'false')
      .set('id', value._id)
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }
    return this.http.put<any>(`${this.baseUrl}address`, payload, httpOptions)
  }

  orderHistory(page, count, value) {
    const data =
    {
      "page": page == 'undefined' ? 1 : page,
      "count": count,
      "type": value
    }
    console.log("authersiveccount", data);
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }
    return this.http.post<any>(`${this.baseUrl}orderHistory`, data, httpOptions)
  }

  orderHistoryPast(page, count) {
    const data =
    {
      "page": page,
      "count": count,
      "type": "past"
    }
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }
    return this.http.post<any>(`${this.baseUrl}orderHistory`, data, httpOptions)
  }

  placedorder(data) {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }
    return this.http.post<any>(`${this.baseUrl}checkout`, data, httpOptions);
  }

  searchproduct(data) {
    return this.http.post<any>(`${this.baseUrl}search_product`, data);
  }

  getcategoryTablist(id) {
    const payload = new HttpParams()
      .set('category', id)
    return this.http.post<any>(`${this.baseUrl}getCategoryTab`, payload);
  }

  getAllRating(data) {
    return this.http.post<any>(`${this.baseUrl}getRatingReviews`, data)
  }

  getorderrepeated(id) {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }
    const payload = new HttpParams()
      .set('orderId', id)
    return this.http.post<any>(`${this.baseUrl}repeatOrder`, payload, httpOptions)
  }

  getshippingCharge(data) {
    return this.http.post(`${this.baseUrl}getCharges`, data)
  }

  defaultaddress(data) {
    return this.http.post(`${this.baseUrl}defaultAddress`, data)
  }

  getproductfeedback(data) {
    const authorization = localStorage.getItem('confirmauth');
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': authorization })
    }
    const payload = new HttpParams()
      .set('product', data.productId)
      .set('rating', data.rating)
      .set('review', data.review)
      .set('orderId', data.orderId)


    return this.http.post<any>(`${this.baseUrl}addProductReview`, data, httpOptions);
  }

  getSearchresult(data) {
    return this.http.post<any>(`${this.baseUrl}search`, data)
  }

  celebrityDetails(data) {
    return this.http.post<any>(`${this.baseUrl}celebrityDetail`, data);
  }

}





