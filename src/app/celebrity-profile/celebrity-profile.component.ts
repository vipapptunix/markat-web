import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {TranslateService} from '@ngx-translate/core';
import {SlidesOutputData, OwlOptions } from 'ngx-owl-carousel-o';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-celebrity-profile',
  templateUrl: './celebrity-profile.component.html',
  styleUrls: ['./celebrity-profile.component.scss']
})
export class CelebrityProfileComponent implements OnInit {

isloggin:any;
imgurl:any;
  mokabookPayStatus: any;
  language: any;
  lang: any;
  celebid: any;
  homedata: any;
  featuredata: any;
  celebritydata: any;
  picksdata: any;
  branddata: any;
  sellerdata: any;
  Isloader:boolean = true;
  offerdata: any;
  purchasedata: any;
  bannerdata: any;
  productId: any;
  prodarray: any;
  customRecommended: { rtl: boolean; loop: boolean; mouseDrag: boolean; freeDrag: boolean; autoplay: boolean; touchDrag: boolean; pullDrag: boolean; dots: boolean; dotsEach: boolean; nav: boolean; slideBy: number; autoWidth: boolean; navText: string[]; responsive: {}; };
  customOptions: { rtl: boolean; loop: boolean; mouseDrag: boolean; touchDrag: boolean; pullDrag: boolean; dots: boolean; dotsEach: boolean; autoWidth: boolean; freeDrag: boolean; autoplay: boolean; navSpeed: number; navText: string[]; responsive: {}; nav: boolean; };

  constructor( private router: Router,
    public authenticationService: AuthenticationService,
    private commonService: CommonService,
    private route: ActivatedRoute, 
    private snackmsg:MatSnackBar,
    private translate:TranslateService,
    private _route: ActivatedRoute,private spinner: NgxSpinnerService) 
    {
      this.imgurl=this.authenticationService.imageUrl;
      this.route.queryParams.subscribe(params => {
        this.celebid = params['id']
      });
      this.isloggin = localStorage.getItem("sucess");
    }

  ngOnInit(){
    this.language = localStorage.getItem('lan')
    this.getcelebrityData()
    this.translate.setDefaultLang(this.language);

    this.customOptions = {
      rtl:this.language == 'arb' ? true : false,
      loop: true,
      mouseDrag: true,
      touchDrag: true,
      pullDrag: true,
      dots: false,
      dotsEach:true,
      autoWidth:false,
      freeDrag:true,
      autoplay:false,
      navSpeed: 70,
      navText: ['',''],
      responsive: {
        0: {
          items: 1
        },
        400: {
          items: 1
        },
        740: {
          items: 1
        },
        940: {
          items: 1
        }
      },
      nav: false
    }
  
    this.customRecommended = {
      rtl:this.language == 'arb' ? true : false,
      loop: false,
      mouseDrag: true,
      freeDrag:true,
      autoplay:false,
      touchDrag: true,
      pullDrag: true,
      dots: false,
      dotsEach:false,
      nav: true,
      slideBy: 5,
      autoWidth:false,
      navText: ['<<', '>>'],
      responsive: {
        0: {
          items: 2
        },
        400: {
          items: 4
        },
        740: {
          items: 4
        },
        940: {
          items: 4
        }
      },
     
    }

  }

  getcelebrityData()
  {
    const data = {"celebrity":this.celebid, "userId":localStorage.getItem('userid'),"page":1}
    this.authenticationService.celebrityDetails(data).subscribe((res)=>
    {
      if(res.data[0].label == 'Home Banner')
      {
         this.homedata = res.data[0].value
      }
      if(res.data[1].label == 'Purchase By Category')
      {
       this.featuredata = res.data[1].value
      }
      if(res.data[5].label == 'Bottom Banners')
      {
       this.celebritydata = res.data[5].value
      }
      if(res.data[4].label == 'Celebrity Endorsed')
      {
        this.picksdata = res.data[4].value
      }
      if(res.data[6].label == 'Sellers Related With Products')
      {
        this.Isloader = false;
        this.branddata = res.data[6].value
      }
      if(res.data[2].label == 'Top Picks')
      {
        this.purchasedata = res.data[2].value
      }
      if(res.data[3].label == 'Banner')
      {
        this.Isloader = false;
        this.bannerdata = res.data[3].value
      }
    })
  }

  bannerpage(data)
  {

    data.type="bannerlist"
    //data.push(obj);
    // this.authenticationService.getbannerdata(data);
     sessionStorage.setItem('bannerlist',JSON.stringify(data.offer))
      this.router.navigate(['/categories'],{ queryParams: { 'type': 'Homebannere' , 'id':data._id ,'celeb':'adver'}})
  
  }

  gotoproductDetails(id)
  {
    window.open(`http://appgrowthcompany.com/markat_web/#/celebrityprofile?id=${id}`,'_blank')
  }

  gotoproduct(id)
  {
    window.open(`http://appgrowthcompany.com/markat_web/#/productdetail?id=${id}`,'_blank')
  }

  addfavlist(id,value)
  {
    if(this.isloggin != null)
    {
      const data =
      {
       "product":id,
        "userId":localStorage.getItem('userid')
      }
      this.snackmsg.open('Loading....','Wishlist',{
       duration: 8000,
     })
     this.authenticationService.postfavlist(data).subscribe((res:any)=>
     {
       if(res.success)
       {
         this.productId = res.data.product
         if(value == 1)
         {
          this.prodarray = this.picksdata.filter(element => element._id == this.productId)
         }
         else
         {
          this.prodarray = this.purchasedata.filter(element => element._id == this.productId)
         }
         this.prodarray[0].isFavourite = !this.prodarray[0].isFavourite
         if(this.prodarray[0].isFavourite)
         {
          this.snackmsg.open(this.language == 'en' ? 'Added to wishlist' : 'أضيف لقائمة الأماني',this.language == 'en' ? 'Wishlist' : 'قائمة الرغبات',{
            duration: 3000,
          }).onAction().subscribe((res)=>
          {
            this.router.navigate(['/wishlist'])
          })
         }
         if(!this.prodarray[0].isFavourite)
         {
          this.snackmsg.open(this.language == 'en' ? 'Removed to wishlist' : 'تمت إزالته من قائمة الرغبات',this.language == 'en' ? 'Wishlist' : 'قائمة الرغبات',{
            duration: 3000,
          }).onAction().subscribe((res)=>
          {
            this.router.navigate(['/wishlist'])
          })
         }
    
       }
     });
    }
    else
    {
      this.snackmsg.open(this.language == 'en' ? 'Please login first!' : 'الرجاء تسجيل الدخول أولا!',this.language == 'en' ? 'Login' : 'تسجيل الدخول',
      {
       duration: 4000,
     }).onAction().subscribe((res)=>
      {
        this.router.navigate(['/login'])
      })
    }
  }
}
