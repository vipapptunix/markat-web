import { JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from '../services/auth.service';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  imgUrl:any;
  checkdata:any;
  countcart = 0
  total_price = 0;
  i:any;
  amount:any;
  isloggin:any;
  price_total:any;
  subprice_total:any;
  public cartdata:any;
  pricedata:any;
  Guestdata:any = [];
  Userdata:any = [];
  prod_id: any;
  idcheck: any;
  language: string;
  lang: any;
  finalcart: any;
  shipping: any;
  tax: any;
  currency: any;
  checkstats: any;
  discount: any;
  taxprice: any;
  total: any;
  sub_total: any;
  shippingprice: any;
  constructor(private router: Router, private snackmsg:MatSnackBar,private translate:TranslateService , public authservice:AuthenticationService) 
  { 
    this.authservice.languagechange.subscribe((res:any)=>
    {
      this.language = res;
    });
  }

  ngOnInit() 
  {
    this.language = localStorage.getItem('lan')
    this.translate.use(this.language) 
    this.isloggin = localStorage.getItem("sucess");
    if(this.isloggin != null)
    {
       document.getElementById('online').style.display = "block";
       document.getElementById('guest').style.display = "none";
    }
    else
    {
     document.getElementById('online').style.display = "none";
     document.getElementById('guest').style.display = "block";
    }
    if(this.isloggin == null)
    {
       this.idcheck = JSON.parse(localStorage.getItem('cartcount'))
       this.idcheck.forEach(element => 
        {
         this.total_price = this.total_price + element.amount
         this.subprice_total = this.total_price;
         this.price_total = this.total_price;
         const data= {"product":element.productId}
         this.authservice.productDetail(data).subscribe((res: any) => {
         res.data.quantity = element.quantity
         this.Guestdata.push(res.data);
           });         
       });
    }
    this.imgUrl = this.authservice.imageUrl;
    if(this.isloggin != null)
    {
   this.getcartCount();
    }
  }


  ngAfterViewInit() {
    this.authservice.gettranslation().subscribe((res: any) => {
      this.discount = res.data.data["discount"];
      this.taxprice = res.data.data["tax"];
      this.total = res.data.data["total"];
      this.sub_total = res.data.data["sub_total"];
      this.shippingprice = res.data.data['shipping']
    });
  }


  goTocheckout()
  {
    this.router.navigate(['/checkout']);
  }

  gotohome()
  {
    this.router.navigate(['/home']);
  }

  removecart(id,status)
  {
    if(status == 'ON')
    {
      this.authservice.removecarddata(id).subscribe((data:any)=>{
          if(data.success)
          {
          this.getcartCount()
          }
        });
    }
    if(status == 'OFF')
    {
      if(this.idcheck.length > 1)
      {
        this.idcheck =  this.idcheck.filter(ele => ele.productId != id)
        localStorage.setItem('cartcount',JSON.stringify(this.idcheck))
        this.idcheck.forEach(element => {
        this.authservice.productDetail(element.product).subscribe((res: any) => {
        res.data.quantity = element.quantity
        this.Userdata.push(res.data);
            })         
        });
          this.Guestdata = this.Userdata
          //window.onload
      }
      if(this.idcheck.length == 1)
      {
        this.idcheck =  this.idcheck.filter(ele => ele.productId != id)
        localStorage.setItem('cartcount',JSON.stringify(this.idcheck))
        this.Guestdata = [];
        this.router.navigate(['/home'])
      }
  
    }

  }


  saveForresult(id)
  {
    const data =
    {
      "product":id,
      "wishlist": true
    }
    this.authservice.savelater(data).subscribe((res:any)=>
    {
      this.getcartCount();
    })
  }

  getcartCount()
  {
      this.authservice.Viewcartlist().subscribe((res:any)=>
      {
     if(this.language == 'en') if(res.message != 'Cart is empty')
      {
       this.cartdata = res.data.cartUser;
       this.finalcart = res.data.finalCart
       this.shipping = this.finalcart.shippingCharges
       this.tax = this.finalcart.taxPercentage
       this.currency = res.data.cartUser[0].product.currency
       this.price_total = this.finalcart.finalAmount
       this.subprice_total = this.finalcart.preTaxAmount
       this.authservice.getcartlength(this.finalcart.cartCount);
      }
      if(this.language == 'arb')  if(res.message != 'عربة التسوق فارغة')
      {
       this.cartdata = res.data.cartUser;
       this.finalcart = res.data.finalCart
       this.shipping = this.finalcart.shippingCharges
       this.tax = this.finalcart.taxPercentage
       this.currency = res.data.cartUser[0].product.currency
       this.price_total = this.finalcart.finalAmount
       this.subprice_total = this.finalcart.preTaxAmount
       this.authservice.getcartlength(this.finalcart.cartCount);
      }
      else
      {
        this.cartdata = [];
        this.authservice.getcartlength('0');
  
      }
     // this.getcartcount(res.data.length);
    })
  }

  subquantity(cartdata,id)
  {
  this.prod_id =cartdata.product._id;
    this.i = cartdata.quantity - 1;
    if(this.i != 0)
    {
      this.authservice.updatelist(cartdata,this.i,cartdata.product.price).subscribe((res:any)=>
      {
        if(res.status)
        {
          this.getcartCount()
        }
      })
    }
  }


  addquantity(cartdata,id)
  {
    this.checkstats = this.cartdata.filter(i => i._id == id)
    this.i = this.checkstats[0].quantity + 1;
    this.prod_id =this.checkstats[0].product._id;
    if(this.i <= this.checkstats[0].product.purchaseQuantity)
    {
      this.authservice.updatelist(cartdata,this.i,cartdata.product.price).subscribe((res:any)=>
      {
        if(res.status)
        {
         this.getcartCount() 
        }
      })
    }
  }

  
  offlineAdd(id)
  {
    this.checkdata = this.Guestdata.filter(i=>i._id == id)[0];
    this.prod_id = this.checkdata._id;
    this.i = this.checkdata.quantity + 1;

    if(this.checkdata.purchaseQuantity >= this.i)
    {    
      this.checkdata.quantity = this.i;
    this.amount = this.checkdata.amount + this.checkdata.price
    this.pricedata = this.checkdata.price;
    this.idcheck.forEach(element => {
      if(element.product == id)
      {
        element.quantity = this.i
      }
    });
    localStorage.setItem('cartcount',JSON.stringify(this.idcheck))
    }
    this.guestcartcount(this.idcheck.length)
  }

  offlineSub(cartdata)
  {
    this.checkdata = this.Guestdata.filter(i=>i._id == cartdata._id)[0];
    this.prod_id = this.checkdata._id;
    this.i = this.checkdata.quantity - 1;

    if(this.i != 0)
    {
      this.checkdata.quantity = this.i; 
      this.amount = cartdata.amount - cartdata.price
      this.idcheck.forEach(element => {
        if(element.product == cartdata._id)
        {
          element.quantity = this.i
        }
      });
      localStorage.setItem('cartcount',JSON.stringify(this.idcheck))
    }
    this.guestcartcount(this.idcheck.length)
  }

  goTologin() 
  {
    this.router.navigate(['/login']);
    }
    goTosignup() {
      this.router.navigate(['/signup']);
      }
  

  getcartcount(count)
  {
   
  }

  guestcartcount(count)
  {
    
    //this.countcart = 0;
    this.price_total = 0;
    for(var i =0;i<count;i++)
    {
     // this.countcart = this.Guestdata[i].quantity + this.countcart;
     // this.price_total = ((this.Guestdata[i].price * this.Guestdata[i].quantity) - (this.Guestdata[i].price * this.Guestdata[i].quantity * (this.Guestdata[i].discount)/100)) + this.price_total;
     this.price_total = this.price_total + this.Guestdata[i].discountedPrice 
   
    }
  }

  Help()
  {
    this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
  }

  // languageChange(lan)
  // {
  //   this.translate.use(lan)
  //   localStorage.setItem('lan',lan)
  //   this.lang = lan
   
  //   this.authservice.languageChange(lan)
  // }

  gotoWhatsapp()
  {
    window.open('https://api.whatsapp.com/send?phone=+918288932697')
  }
}
