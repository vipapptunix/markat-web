import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-failure',
  templateUrl: './failure.component.html',
  styleUrls: ['./failure.component.scss']
})
export class FailureComponent implements OnInit {
  language: string;

  constructor(private router:Router,private route:ActivatedRoute,private translate:TranslateService) { }

  ngOnInit() {
    this.language = localStorage.getItem('lan')
  }

  gotohomepage()
  {
    this.router.navigate(['/home']);
  }
}
