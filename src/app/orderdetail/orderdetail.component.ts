import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { CanceldialogboxComponent } from '../canceldialogbox/canceldialogbox.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
//import { StarRatingComponent } from 'ng-starrating';
import {TranslateService} from '@ngx-translate/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { CommonService } from '../services/common.service';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-orderdetail',
  templateUrl: './orderdetail.component.html',
  styleUrls: ['./orderdetail.component.scss']
})
export class OrderdetailComponent implements OnInit {

  public history_id :any; 
  public historyord:any;
  historyordpast:any;
  ratingvalue:any;
  status:any;
  Reciept:any;
  feedback:any;
  newratingvalue:any;
  ratingid:any;
  repreatedid:any;
  orderratingid:any;
  pagecount:any;
  type:any;
  public form: FormGroup;
  rating3:any;
  language: any;
  constructor(private snackmsg:MatSnackBar,private router: Router,private commonService: CommonService,private translate:TranslateService,private authSrv:AuthenticationService,public dialog: MatDialog,private fb: FormBuilder)
   { 
    this.rating3 = 0;
    this.form = this.fb.group({
      rating: ['', Validators.required],
    })
   }

  ngOnInit() {
    this.language = localStorage.getItem('lan')
    this.translate.setDefaultLang(this.language);
    this.authSrv.languagechange.subscribe((res:any)=>
 {
   this.language = res;
   
   console.log(res);
 })
 
    this.authSrv.historyid.subscribe((res:any)=>{

      this.history_id = res.id;
      this.pagecount = res.pagecount;
      this.type = res.type
    })
    if(this.type  == 'ongoing')
    {
      this.authSrv.orderHistory(this.pagecount,100,'ongoing').subscribe((res:any)=>{
        this.historyord= res.data
     
       this.historyord =this.historyord.filter(id => id._id == this.history_id);
       this.status = this.historyord[0].status
  
      });
    }
    if(this.type == 'past')
    {
      this.authSrv.orderHistoryPast(this.pagecount,100).subscribe((res:any)=>
      {
        this.historyordpast = res.data
        
        
        this.historyordpast = this.historyordpast.filter(id => id._id == this.history_id);
        this.status = this.historyordpast[0].status
     
      })
    }
    this.language = localStorage.getItem('lan')
  }
  goToorderdetail()
  {
    this.router.navigate(['/orderdetail']);
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(CanceldialogboxComponent, {
     
    });
  }
  backtohome(e)
  {
    if(e == 1)
    {
      this.router.navigate(['/home']);

    }
    if(e == 2)
    {
       this.router.navigate(['/profile'])
    }
  }

  orderrepeated(id)
  {
    this.repreatedid = id;
  }

  submitrepreated()
  {
    ;
    this.authSrv.getorderrepeated(this.repreatedid).subscribe((res:any)=>
    {
      
      if(res.status)
      {
        this.commonService.showToasterSuccess(this.language == 'en' ? "order has been placed!" : 'تم وضع الطلب!')
          this.router.navigate(['/profile']);
      }
    })
  }

  checkorderid(id,orderid)
  {
    
    this.ratingid = id;
    this.orderratingid = orderid;
  }

  submitrating()
  {
     const data = 
     {
       "product":this.ratingid,
       "rating": this.ratingvalue,
       "review":this.feedback,
       "orderId": this.orderratingid
     }
     console.log("",data);
     
     if(this.feedback != undefined && this.ratingvalue != undefined)
     {
      this.authSrv.getproductfeedback(data).subscribe((res:any)=>
      {
        this.commonService.showToasterSuccess(this.language == 'en' ? "Thanks for your feedback!." : 'شكرا لملاحظاتك!.');
        console.log("::::",res);
        this.router.navigate(['/profile']);
      })
     }if(this.feedback === undefined)
     {
       this.commonService.showToasterError(this.language == 'en' ? 'please enter feedback!' : 'الرجاء إدخال الملاحظات!')
      //  this.snackmsg.open('please enter feedback!','Missing',{duration:3000})
     }
     if(this.ratingvalue === undefined)
     {
      this.commonService.showToasterError(this.language == 'en' ? 'please select the rating..' : 'الرجاء تحديد التصنيف ..')
      // this.snackmsg.open('please select the rating..','',{duration:3000})
     }
     
  }

  ratingcheck(e)
  {
    console.log(e)
    this.ratingvalue = e;
  }
  getReciept(id)
  {
    // this.authSrv.printReciept(id).subscribe((res:any)=>
    // {
    //   this.Reciept = atob(res.data);
    //  console.log(this.Reciept)
    window.open(`http://docs.google.com/gview?embedded=true&url=https://appgrowthcompany.com:9079/api/app/receipt?orderId=${id}`);
    //  win.document.body.innerHTML=this.Reciept;
    // })
  }

  contactUs()
  {
    this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
  }
}
