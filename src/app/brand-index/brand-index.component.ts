import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';

@Component({
  selector: 'app-brand-index',
  templateUrl: './brand-index.component.html',
  styleUrls: ['./brand-index.component.scss']
})
export class BrandIndexComponent implements OnInit {
  array = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
  imgurl: string;
  celebrity: any;
  feature: string;
  language: string;
  alphabate: string;
  constructor(private Srvc:AuthenticationService,private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.language = localStorage.getItem('lan')
    this.imgurl=this.Srvc.imageUrl;
    this.getBranddata();
  }

  getBranddata()
  {
    const data =
    {
      'page': 1,
      'count':100,
      "isFeatured":true,
      'albhabet': this.alphabate == '' ? '' : this.alphabate
    }
  this.Srvc.getBrandtab(data).subscribe((res:any)=>
  {
    this.celebrity = res.data;
  })
  }

  getAllcelebrity(item)
{
 if(item == '')
 {
 
   this.feature = 'all'
   this.alphabate = item;
   this.getBranddata() 
 }
   if(item != ''){
    this.alphabate = item;
     this.getBranddata()
    }
}

gotforbrand(id)
{
  this.router.navigate(['/categories'],{queryParams:{'id':id,'type':'brand'}})
}
}
