import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-verificationby',
  templateUrl: './verificationby.component.html',
  styleUrls: ['./verificationby.component.scss']
})
export class VerificationbyComponent implements OnInit {
  input: any;
  phone: any;
  guestprod: any;
  isspinner:boolean = false;
  constructor(private Srvc:AuthenticationService,private router: Router, private commonService: CommonService,private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
       this.phone = params['phone']
    })
  }

  resendVerification()
  {
    const data =
    {
      "phone":this.phone,
      "countryCode":'+91',
      "otp":this.input,
      "type":'signupVerification'
    }
    this.Srvc.verifyPhone(data).subscribe((res:any)=>
    {
       if(res.success)
       {
        
        localStorage.setItem("confirmauth", res.data.accessToken);
        localStorage.setItem("sucess",res.success);
        sessionStorage.setItem('x-auth',"1")
          this.guestprod = JSON.parse(localStorage.getItem('cartcount'))
          if(this.guestprod)
          {
         
            this.guestprod.forEach(element => {
              element.amount = element.amount * element.quantity
            });
            const datacheck = {
              "data": this.guestprod
            }
            console.log("------------>",datacheck)
          
          }
          setTimeout(() => {
            this.router.navigate(['/home']);
          }, 1000);
       }
       else
       {
         this.commonService.showToasterError(res.message)
       }
    })
  }

  onOtpChange(event)
  {
    this.input = event
  }

  SendAgain()
  {
    const data=
    {
      "phone":this.phone,
      "countryCode":"+91"
    }
   this.Srvc.reSendotp(data).subscribe((res:any)=>
   {
     this.commonService.showToasterSuccess(res.message)
   })
  }

}

