import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationbyComponent } from './verificationby.component';

describe('VerificationbyComponent', () => {
  let component: VerificationbyComponent;
  let fixture: ComponentFixture<VerificationbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificationbyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
