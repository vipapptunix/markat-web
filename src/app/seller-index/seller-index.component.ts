import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';

@Component({
  selector: 'app-seller-index',
  templateUrl: './seller-index.component.html',
  styleUrls: ['./seller-index.component.scss']
})
export class SellerIndexComponent implements OnInit {
  array = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
  celebrity: any;
  imgurl: string;
  isloader = true;
  isfeature: any;
  seller: any = "";
  feature: string;
  celebid: any;
  language: string;
  constructor(private Srvc:AuthenticationService,private router: Router, private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      this.isfeature = params['feature']
      this.celebid = params['id']
    });
   }

  ngOnInit() {
    this.language = localStorage.getItem('lan')
    this.imgurl=this.Srvc.imageUrl;
    this.getAlldata('','')
  }

  goTocelebrityprofile(item) {
    console.log("bestseller",item)
    this.router.navigate(['/celebrityprofile'],{queryParams:{'id':item._id}});
  }

  // getAlldata(item)
  // {
  //   const data =
  //   {
  //     "page":1,
  //     "count":10,
  //   }
  //  this.Srvc.sellerCelibrity(data).subscribe((res:any)=>
  //  {
  //   this.celebrity = res.data;
  //  }) 
  // }

  // CheckSeller(item)
  // {
  //   this.getAlldata(item)
  // }

  getAlldata(item,value)
{
  if(this.feature == 'all')
  {
    const data=
    {
      "page":1,
      "count":10,
      "albhabet": item
    }
    this.Srvc.sellerCelibrity(data).subscribe((res:any)=>
    {
     // this.isloader = false;
       this.celebrity = res.data
       //this.celebcount = res.data.length
       
    })
  }
  if(this.feature != 'all')
  {
    const data=
    {
      "page":1,
      "count":10,
      "isFeatured":value,
      "albhabet": item
    }
    this.Srvc.sellerCelibrity(data).subscribe((res:any)=>
    {
     // this.isloader = false;
       this.celebrity = res.data
       //this.celebcount = res.data.length
       
    }) 
  }

}

CheckSeller(item)
{
  this.isloader = true;
 if(item == '')
 {
   this.feature = 'all'
   this.getAlldata(item,'') 
 }
   if(item != '')this.getAlldata(item,this.isfeature)
}
}
