import { JsonpClientBackend } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from '../services/auth.service';
declare const $: any;
@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.scss']
})
export class CompareComponent implements OnInit {
  comparearray: any;
  comparevalue: any;
  imgurl:any;
  GuestData:any;
  guestArray:any[] = [];
  isloggin: string;
  index: number;
  getarray: string;
  language: string;
  constructor(private Srvc:AuthenticationService,private router: Router,private translate:TranslateService) { 
    this.comparearray = JSON.parse(sessionStorage.getItem('compareid'))
  }

  ngOnInit() {
    this.language = localStorage.getItem('lan')
    this.isloggin = localStorage.getItem("sucess");
    this.imgurl = this.Srvc.imageUrl
    const data=
    {
      "products":this.comparearray
    }
    this.Srvc.getCompareprod(data).subscribe((res:any)=>
    {
      this.comparevalue = res.data
    })
  }

  goTocart(cart) {

    if (!this.isloggin) {
    
      const data =
          {
            "productId": cart._id,
            "amount": cart.price - (cart.price*cart.discount)/100,
            //"amount": (this.productDetails.price - ((this.productDetails.price*this.productDetails.discount)/100)),
            "quantity": 1
          }
        if(localStorage.getItem('cartcount'))
        {
          this.guestArray = JSON.parse(localStorage.getItem('cartcount'))
          if(this.guestArray)
          {
            var sum = 0;
            this.guestArray.forEach(element => {
              if(element.productId == cart._id)
              {
                element.quantity = element.quantity + 1;
                console.log("inside")
              }
          
            });
            if(this.guestArray.filter(ele => ele.productId == cart._id).length < 1)
            {
              this.guestArray.push(data)
            }
          }
          else
          {
              this.guestArray.push(data);
          }
          
          
        }else
        {
          this.guestArray.push(data)
        }
      this.router.navigate(['/cart']);

      console.log("GuestDAta",this.GuestData)
     
      localStorage.setItem('cartcount',JSON.stringify(this.guestArray))
    }
    else {
      const pstdata =
      {
        "data": [
          {
            "product": cart._id,
            //"amount": this.cartlist.price,
            //"amount": (this.productDetails.price - ((this.productDetails.price*this.productDetails.discount)/100)),
            "quantity": 1
          }
        ]
      }
      this.Srvc.postcartlist(pstdata).subscribe((res: any) => {

        if (res.status) {
          this.router.navigate(['/cart']);
        }
      });
    }


  }

  clearComp(id)
  {
    const index = this.comparearray.indexOf(id)
    this.comparearray.splice(index, 1);
    sessionStorage.setItem('compareid',JSON.stringify(this.comparearray))
    location.reload()
  }
}
