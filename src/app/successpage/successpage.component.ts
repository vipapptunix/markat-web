import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-successpage',
  templateUrl: './successpage.component.html',
  styleUrls: ['./successpage.component.scss']
})
export class SuccesspageComponent implements OnInit {
  language: string;

  constructor(private router:Router,private route:ActivatedRoute) {this.language = localStorage.getItem('lan') }

  ngOnInit() {
  }


  gotohome()
  {
  this.router.navigate(['/home']);
  }
}
