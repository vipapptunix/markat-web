import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef, AfterViewInit, OnChanges, SimpleChanges, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogboxComponent } from '../dialogbox/dialogbox.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AuthenticationService } from '../services/auth.service';
import { NotificationService } from '../services/notification.service';
import { TranslateService } from "@ngx-translate/core";
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import * as js from '../../assets/js/custom';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormControl, FormControlName, FormGroup } from '@angular/forms';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
declare var google: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit, OnChanges {
  @ViewChild('addressInput',{static:false })
addressInput: ElementRef;
  authToken: any;
  isloggin: any;
  bannerData: any;
  address_value:any;
  categoryData: any;
  public homeData: any;
  public count: any;
  profiledata: any;
  searchtxt: any;
  searchvalue: any;
  Cityname:any;
  googleid: any;
  firstname: any;
  cartdata: any;
  googlevalue: any;
  countcartlen: any;
  lang: any;
  countcart: any;
  categoryDataVal: any;
  search_result: any;
  bannerlist: any;
  subcategeorylist: any;
  listsubcategory: any;
  categoryid: any;
  sublength: any;
  imgurl: any;
  language: any = 'en';
  profilepicdata: any;
  @Output() searchtextvalue = new EventEmitter();
  imgflag: string;
  checktoken: string;
  searchval: any;
  countryFlg: any;
  arrayImg: any;
  countryC: any;
  countryCode: string;
  lat: any;
  lng: any;
  state: any;
  country: any;
  checlatlng: string;
  form: FormControl;
  @ViewChild("placesRef", { static: true }) placesRef: GooglePlaceDirective;
  EASY_RETURNS: any;
  SECURE_SHOPPING: any;
  trusted_shipping: any;

  
  constructor(
    private router: Router, 
    private route: ActivatedRoute, 
    private snackmsg: MatSnackBar, 
    private dialog: MatDialog, 
    private translate: TranslateService,
    private readonly _formBuilder: FormBuilder,
    private ngZone: NgZone,
    public matDialog: MatDialog, 
    private authservice: AuthenticationService, 
    private notify: NotificationService) {
    
  //  this.getAllcode()
    translate.addLangs(["en", "arb"]);
    translate.setDefaultLang('en');

    this.countryC = localStorage.getItem('cou') == null ? 'US' : localStorage.getItem('cou');
    this.authservice.googlefbdata.subscribe((res: any) => {
      this.googleid = res.id;

    })
    this.authToken = JSON.parse(localStorage.getItem('authToken'));
    this.isloggin = localStorage.getItem("sucess");
    this.checktoken = localStorage.getItem('gender')

    this.authservice.$profilepic_check.subscribe((res: any) => {
      this.profilepicdata = res.profilePic;
      this.firstname = res.firstName;

    })
    this.imgurl = this.authservice.imageUrl;
    // this.notify.setupSocketConnection();
    //this.route.queryParams.subscribe(params => {this.language = params['lan']});
    this.language = localStorage.getItem('lan')
    this.countryCode = localStorage.getItem('cou') == '' ? 'US' : localStorage.getItem('cou')
    // this.imgflag = this.language == 'en' ?  'assets/images/eng_flag.jpg' : 'assets/images/arb_flag.jpg' ;
    if (this.language == 'en') this.imgflag = 'assets/images/eng_flag.jpg'
    if (this.language == 'arb') this.imgflag = 'assets/images/arb_flag.jpg'

    this.authservice.gettranslation().subscribe((res: any) => {
      this.EASY_RETURNS = res.data.data["EASY_RETURNS"];
      this.SECURE_SHOPPING = res.data.data["SECURE_SHOPPING"];
      this.trusted_shipping = res.data.data["trusted_shipping"];
    });

  
      navigator.geolocation.getCurrentPosition(pos => {
        this.lng = +pos.coords.longitude;
        this.lat = +pos.coords.latitude;
        this.getAddress(this.lat,this.lng)

      });
   
   
  }

  ngOnChanges(changes: SimpleChanges): void {
     this.address_value='check Again'
  }

  selected = 'option1';
  ngOnInit() {

    this.translate.setDefaultLang(this.language);
    if (localStorage.getItem('provider') == 'GOOGLE' || localStorage.getItem('provider') == 'FACEBOOK') {
      this.authservice.Userdata.subscribe(userdata => {
        if (userdata.success) {
          this.firstname = userdata.data.firstName;
        }
      });
    }


    // console.log(":",this.isloggin);
    if (localStorage.getItem("sucess")) {
      this.authservice.getprofile().subscribe((profdata: any) => {
        this.profilepicdata = profdata.data.profilePic;
        this.profiledata = profdata.data._id;
        this.firstname = profdata.data.firstName;

      });


      this.authservice.cartlen.subscribe((res: any) => {
        if(res != "")
        {
          this.countcartlen = res;
          this.count = this.countcartlen;
        }
        else
        {
          this.countcartlen = 0
          this.count = 0;
        }
       
      })

      this.authservice.homeprofname.subscribe((res: any) => {
        //when profile page setup it set value to my account.
      })
      this.getcartCount()
    }

    this.getDashboard();

    if (this.isloggin == null && JSON.parse(localStorage.getItem('cartcount')) == undefined ? '' : JSON.parse(localStorage.getItem('cartcount')).length > 0) {
      this.countcartlen = JSON.parse(localStorage.getItem('cartcount')).length
    }
  
  }

 // js.customscript();

  languageChange(lan) 
  {
    this.translate.use(lan)
    localStorage.setItem('lan', lan)
    this.lang = lan
    if (this.lang == 'en') this.imgflag = 'assets/images/eng_flag.jpg';
    if (this.lang == 'arb') this.imgflag = 'assets/images/arb_flag.jpg'
    this.authservice.languageChange(lan)
    //this.router.navigate([],{queryParams:{lan}})
  }



  getAddress(latitude,longitude)
  { 
    var geocoder;
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(latitude, longitude);
    geocoder.geocode(
      
      {'latLng': latlng}, 
      function(results, status) {
        
          if (status == google.maps.GeocoderStatus.OK) {
                  if (results[0]) 
                  {
                    console.log(results,'results[0]');
                    
                     this.ngZone.run(() => {
                      this.address_value = results[0].formatted_address
                    })
                  }
          }
          
      }
  );
  }

  getAllcode() {
    this.authservice.getAllcountry().subscribe((res: any) => {
      this.countryFlg = res.data
    })
  }

  getcartCount() {
    this.authservice.Viewcartlist().subscribe((res: any) => {
      this.countcartlen = res.data.finalCart.cartCount
    })
  }

  logindirect() {
    this.router.navigate(['/login'])
  }

  getDashboard() {
    const data = {
      "categories": [],
      "subCategories": [],
      "brands": [],
      "minPrice": 0,
      "maxPrice": 5000,
      "rating": 1
    }
    this.authservice.getCategorieslist(data).subscribe((res: any) => {
      if (res.success) {
        this.categoryDataVal = res.data.categories
      }
    });
  }

  checkCode(event) {
    console.log(event)
    this.arrayImg = this.countryFlg.filter(ele => ele.countryCode == event)
    this.countryC = this.arrayImg[0].countryCode
    localStorage.setItem('cou', this.countryC);
    this.authservice.countryChange('curr');
  }

  Selectshop(event) {
    if (event === 'M') {
      localStorage.setItem('gender', 'F')
      location.reload();
    }
    if (event === 'F') {
      localStorage.setItem('gender', 'M')
      location.reload();
    }
  }

  Help() {
    this.router.navigate(['/terms&cond'], { queryParams: { 'name': 'Help' } })
  }

  public AddressChange(address: any) {
    //setting address from API to local variable
    this.lat = address.geometry.location.lat()
    this.lng = address.geometry.location.lng()
    sessionStorage.setItem('latitude',this.lat);
    sessionStorage.setItem('longitude',this.lng);
    window.location.reload()
    let length = address.address_components.length
    this.state = address.address_components[0].long_name;
  }

  getcategoryId(id, i) {
    this.subcategeorylist = this.categoryDataVal.filter(ele => ele._id == id)
    this.subcategeorylist = this.subcategeorylist[0].subCats
  }

  gotoCategory(id,i) {
    this.router.navigate(['/categories'], { queryParams: { 'id': id, 'type': 'category', 'sub': '1' } })
    this.authservice.checkproductId(id)
  }

  getallcatergory(id) {
    this.router.navigate(['/categories'], { queryParams: { 'id': id, 'type': 'category', 'sub': '0' } })
    this.authservice.checkproductId(id)
  }
  goTocart() {
    this.router.navigate(['cart']);
  }
  goTohome() {
    this.router.navigate(['/home']);
  }

  headerpage(id) {
    this.authservice.tranferhistoryid(id);
    this.router.navigate(['/headerpage']);
  }

  goTosignup() {
    this.router.navigate(['/signup']);
  }

  goTologin() {
    this.router.navigate(['/login']);
  }

  goTocheckout() {
    this.router.navigate(['/checkout']);
  }
  //Addacoount

  goToprofile() {
    if (this.isloggin) {
      this.router.navigate(['/profile']);
    }
    else {}
  }


  goTowishlist() {
    if (this.isloggin != null) {
      this.router.navigate(['/wishlist']);
    }
    else {
      this.snackmsg.open(this.language == 'en' ? 'Please login first!' : 'الرجاء تسجيل الدخول أولا', this.language == 'en' ? 'Login' : 'تسجيل الدخول',
        {
          duration: 4000,
        }).onAction().subscribe((res) => {
          this.router.navigate(['/login'])
        })
    }
  }

  goTomyProfile(value) {
    if (value == 1) {
      this.router.navigate(['/login']);
    }
    if (value == 2) {
      this.router.navigate(['/signup']);
    }
  }

  getcartcount(count) {
    // console.log(count);
    this.countcart = 0;
    for (var i = 0; i < count; i++) {
      this.countcart = this.cartdata[i].quantity + this.countcart;
    }
    this.authservice.getcartlength(this.countcart);
  }

  searchtext(e) {
    console.log(this.searchval)
    this.searchvalue = this.searchval;
    if (this.searchvalue != null) {
      this.authservice.searchdata(this.searchvalue)
      this.router.navigate(['/categories'], { queryParams: { 'Sval': this.searchvalue, 'result': 'search' } });
    };
  }

  openmodal() {
    document.getElementById('openmodal').click()
  }

  movetocategories() {

    if (this.searchvalue != null) {
      this.authservice.searchdata(this.searchvalue);
      // this.router.navigate(['/categories']);
      const data =
      {
        "limit": 10,
        "skip": 0,
        "searchText": this.searchvalue,
        "userId": "5eb3d95660b4e137e4e0934b"
      }
      this.authservice.searchproduct(data).subscribe((res: any) => {
        if (res.data.length == 0) {
          this.router.navigate(['/notfound']);
        }
        else {
          this.router.navigate(['/categories']);
        }
      });
    }
  }


  gotoWhatsapp() {
    window.open('https://api.whatsapp.com/send?phone=+918288932697')
  }

  getresult() {
    this.authservice.SearchResult().subscribe((res: any) => {
      console.log('res', res)
    })
  }

  openNav() {
    document.getElementById("mySidepanel").style.right = "0px";
  }

  closeNav() {
    document.getElementById("mySidepanel").style.right = "-160px";
  }

}
