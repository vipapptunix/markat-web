import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesdetailComponent } from './categoriesdetail.component';

describe('CategoriesdetailComponent', () => {
  let component: CategoriesdetailComponent;
  let fixture: ComponentFixture<CategoriesdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
