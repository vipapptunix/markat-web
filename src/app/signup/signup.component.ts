import { Component, ElementRef, Inject, NgZone, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import {ProgressSpinnerMode} from '@angular/material/progress-spinner';
import { NgxSpinnerService } from "ngx-spinner";

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
declare var $: any;
declare var google:any
import {
  SearchCountryField,
  TooltipLabel,
  CountryISO
} from "ngx-intl-tel-input";
import { CommonService } from '../services/common.service';
import { AuthenticationService } from '../services/auth.service';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";

export interface DialogData {
  animal: string;
  name: string;
}


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  token = localStorage.getItem("token");
  title: string = "AGM project";
  latitude: number;
  longitude: number;
  message;
  lat: any;
  lng: any;
  zoom: number;
  ProfileForm: FormGroup;
  confirmError: boolean;
  public imagePath;
  isloaded:boolean = false;
  imgURL: any;
  file: any;
  profilePic: any;
  counteryiso:string="CountryISO.India";
  private geoCoder;
  public searchElementRef: ElementRef;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom
  ];
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  SentData:string;
  data : any;
  countryDataArray;
  submitted:boolean=false;
  viewIs;
  countryCodeData=CountryISO.India;
  language: string;
  option: any = '';
  name: any;
  animal: any;
  @ViewChild("placesRef", { static: true }) placesRef: GooglePlaceDirective;
  country: any;
  state: any;
  constructor(private fb: FormBuilder,
    private router: Router,
    public authenticationService: AuthenticationService,
    private commonService: CommonService,
    private _route: ActivatedRoute,private ngZone: NgZone,private translate:TranslateService,public dialog: MatDialog, private spinner :NgxSpinnerService) { this.authenticationService.languagechange.subscribe((res:any)=>
      {
        this.language = res;
      }) }

  ngOnInit() {
    this.language = localStorage.getItem('lan')
    this.translate.use(this.language) 
    
    this.submitted=false;
    this.countryCodeData=CountryISO.India;
    this.createForm();

    this.authenticationService.$chooseoption.subscribe((res:any)=>
    {
      console.log("-------option-",res)
      if(res != '')
      {
        this.option = res;
        this.prepareForm()
      }
    
    })
  }


  public AddressChange(address: any) {
    //setting address from API to local variable
    console.log(address);
    this.lat = address.geometry.location.lat()
    this.lng = address.geometry.location.lng()
    let length = address.address_components.length
    this.country = address.address_components[0].long_name;
    this.state = address.address_components[0].long_name;
    this.ProfileForm.controls['address'].setValue(this.state)

  }
  
  handleAddressChange(address){
    console.log(address);
    // this.address=address.formatted_address;
    // this.latitude=address.geometry.location.lat();
    // this.longitude=address.geometry.location.lng();
  }

   // Get Current Location Coordinates
  //  private setCurrentLocation() {
  //   if ('geolocation' in navigator) {
  //     navigator.geolocation.getCurrentPosition((position) => {
  //       // console.log(position)
  //       this.latitude = position.coords.latitude;
  //       this.longitude = position.coords.longitude;
  //       this.zoom = 15;
  //       this.getAddress(this.latitude, this.longitude);
  //     });
  //   }
  // }

  getAddress(latitude, longitude) {
    console.log("lat-",latitude,"lon-",longitude);
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 15;
          // this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
 
    });
  }


  createForm() {

    this.ProfileForm = this.fb.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      // userName: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      phone: [""],
      address: ["",Validators.required],
      country:[''],
      lng:[''],
      lat:[''],
      password: ["", Validators.required],
      cpassword: ["", Validators.required],
      verificationBy:['']
    });
  }

  checkConfirm(pass, confirmpass) {
    String(confirmpass.value) == String(pass.value);
    this.confirmError = !(String(confirmpass.value) == String(pass.value))
    return String(confirmpass.value) == String(pass.value)
  }

  public errorHandling = (control: string, error: string) => {
    return this.ProfileForm.controls[control].hasError(error);
  }

  onChange(event) {
    var files = event.srcElement.files;
    this.file = files[0];
    if (event.target.files[0].type.indexOf("image/") == 0) {
    this.loadImage(this.file.type, files);
    }
    else{
      this.commonService.showToasterError("Invalid Image!!!");
    }
  }

  loadImage(mimeType, files) {
    console.log("--lasdjfjalsfdj", files);

    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = _event => {
      this.imgURL = reader.result;
      this.profilePic = this.imgURL;
    };
  }


  prepareForm() {
    this.spinner.show()
    this.isloaded = true;
    const controls = this.ProfileForm.controls;
console.log("controls.phone.value.dialCode"+controls.phone.value.dialCode);
   
const userId = 'user001';
    let data = {
      "firstName": controls.firstName.value,
      "lastName": controls.lastName.value,
      "email": controls.email.value,
      "password": controls.password.value,
      "phone": controls.phone.value.number,
      "countryCode": controls.phone.value.dialCode,
      "confirmPassword":controls.cpassword.value,
      "address":controls.address.value,
      "verificationBy":this.option == 1 ? 'phone' : 'email',
      "country":'India',
      "lat":0,
      "lng":0

    };

    let formData = new FormData();
    formData.append("profilePic", this.file);
    formData.append("firstName", controls.firstName.value);
    formData.append("lastName", controls.lastName.value);
    formData.append("email", controls.email.value);
    formData.append("password", controls.password.value);
    formData.append("address",controls.address.value);
    formData.append("verificationBy",this.option == 1 ? 'phone' : 'email');
    formData.append("confirmPassword", controls.cpassword.value);
    formData.append("phone", controls.phone.value.number);
    formData.append("countryCode", controls.phone.value.dialCode);
    formData.append("country",'India'),
    formData.append('lat',this.lat),
    formData.append('lng',this.lng)

    this.completeSignup(formData,data);
  }

  completeSignup(_data,dataemail) {
    
    this.authenticationService.completeRegistration(_data).subscribe(
      (_response: any) => {
        console.log("Response is "+JSON.stringify(_response));
        if(_response.success){
          this.spinner.hide();
          this.isloaded = false;
          this.commonService.showToasterSuccess(_response.message);
      
          if(this.option == 1)
          {
            this.router.navigate(['Verifyphone'], { queryParams: { 'phone': dataemail.phone}});
          }
          if(this.option == 2){
            this.router.navigate(['verfication'], { queryParams: { 'email': dataemail.email }});
          }
          
        }
        else
        {
          this.spinner.hide();
          this.isloaded = false;
          if(!_response.success)
          {
            this.commonService.showToasterError(_response.message)
          }
        }
 
      },
      err => {
        this.spinner.hide();
        this.isloaded = false;
        console.log("error occured"+JSON.stringify(err));
        if (err.error)
          this.commonService.showToasterError(err.error["message"]);
      }
    );
  }

  movetonext()
  {
    this.router.navigate(['/home']);
  }

  testpattern(phone){

    var phonee=phone
var pattern="^[0-9]*$";
return phonee.match(pattern);
  }

  onSumbit() {
    const controls = this.ProfileForm.controls;
    this.submitted=true;
  
    /** check form */
    if (this.ProfileForm.invalid) {
      
      // this.commonService.showToasterError('Please fill required Field appropriately');
      if(this.ProfileForm.controls['phone'].invalid)
      {
        //this.commonService.showToasterError('Phone number is not valid ');
      }
      if(this.ProfileForm.controls['email'].invalid && this.ProfileForm.controls['email'].value != '')
      {
        this.commonService.showToasterError(this.language == 'en' ? 'Email is not valid ' : 'البريد الإلكتروني غير صالح');
      }
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      return;
      
    } 
    // else if(!this.profilePic) {
    //   this.commonService.showToasterError('Please add your profile picture');
    //   return
    // } 
    else {
if(controls["phone"] && controls["phone"].value){
  
  var testvalue=this.testpattern(controls["phone"].value.number);
  if(testvalue){

    if(controls["password"].value==controls["cpassword"].value){

      this.spinner.hide()
      this.isloaded = false;
      //this.openmodaform()
      const dialogRef = this.dialog.open(ChooseOption, {
        width: '250px',
        data: { name: this.name, animal: this.animal }
      });
  
      dialogRef.afterClosed().subscribe(result => {
        //this.router.navigate(['/home']);
        this.animal = result;
      });
    }
    else{
      this.isloaded = false;
      this.spinner.hide()
      this.commonService.showToasterError(this.language == 'en' ? 'Password and confirm password does not match' : 'كلمة المرور وتأكيد كلمة المرور غير متطابقين');
    }
  
  }
  else{
    this.isloaded = false;
    this.spinner.hide()
    this.commonService.showToasterError(this.language == 'en' ? 'Phone number is not valid' : 'رقم الهاتف غير صالح');
   }
}
else{
  this.isloaded = false;
  this.spinner.hide()
  this.commonService.showToasterError(this.language == 'en' ? 'Phone number is not valid ' : 'رقم الهاتف غير صالح');
}
     
    }
  }

  goTologin() {
    this.router.navigate(['login']);
  }
  goToverfication() {
    this.router.navigate(['/verfication']);
  }

 

 
  openmodaform()
  {
    document.getElementById('verify_by').click()
  }

  
}

@Component({
  selector: 'choose-option',
  templateUrl: 'choose-option.html',
})
export class ChooseOption {
  option: any;
  language: string;

  constructor(
    public dialogRef: MatDialogRef<ChooseOption>, private router: Router,private Srvc:AuthenticationService,private translate:TranslateService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {     this.language = localStorage.getItem('lan')
    this.translate.use(this.language) }

  onNoClick(): void {
    this.dialogRef.close();
    this.router.navigate(['/home']);
  }

  selectaddres(id)
  {
    alert(id);
  }

  checkoption(event)
  {
  this.option = event.value;
  }


  finalsubmit()
  {
    console.log('check')
    if(this.option != null)
    {
      this.dialogRef.close();
      this.Srvc.transfersign(this.option);
    //   this.isloaded = true;
    // this.spinner.show()
    }
  }

  //payment gateway//
 
}
