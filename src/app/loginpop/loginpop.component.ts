import { Component, OnInit } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-loginpop',
  templateUrl: './loginpop.component.html',
  styleUrls: ['./loginpop.component.scss']
})
export class LoginpopComponent implements OnInit {
  isloggin: any;

  constructor(private translate:TranslateService,private router:Router) {this.isloggin = localStorage.getItem("sucess"); }

  ngOnInit() {
  }

  goTosignup() {
    this.router.navigate(['/signup']);
  }

  goTologin() {
    this.router.navigate(['/login']);
  }

}
