import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../services/common.service';
import { AuthenticationService } from '../services/auth.service';
import {ProgressSpinnerMode} from '@angular/material/progress-spinner';
import {ThemePalette} from '@angular/material/core';
@Component({
  selector: 'app-verfication',
  templateUrl: './verfication.component.html',
  styleUrls: ['./verfication.component.scss']
})
export class VerficationComponent implements OnInit {
  color: ThemePalette = 'primary';
  mode: ProgressSpinnerMode = "indeterminate";
  value = 50;
  isspinner:boolean= false;
email:any;
  guestprod: any;
  constructor(private router: Router,
    public authenticationService: AuthenticationService,
    private commonService: CommonService,
    private route:ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => { this.email = params['email'] });

    //this.sendverificationLink(this.email);
  }
//   sendverificationLink(email){
//     var data={
//       "email":email
//     }
//     this.authenticationService.sendVerificationLink(data).subscribe((res:any)=>{
// if(res.success){
//   console.log("message is"+JSON.stringify(res));
// }
//     })
//   }

  verifyEmail()
  {
    const data=
    {
      "email":this.email,
      "deviceType":'web',
      "pushToken":''
    }
     this.authenticationService.authCheck(data).subscribe((res:any)=>
     {
      if(res.success)
      {
       
       localStorage.setItem("confirmauth", res.data.accessToken);
       localStorage.setItem("sucess",res.success);
       sessionStorage.setItem('x-auth',"1")
         this.guestprod = JSON.parse(localStorage.getItem('cartcount'))
         if(this.guestprod)
         {
        
           this.guestprod.forEach(element => {
             element.amount = element.amount * element.quantity
           });
           const datacheck = {
             "data": this.guestprod
           }
         }
         setTimeout(() => {
           this.router.navigate(['/home']);
         }, 1000);
      }
      else
      {
        this.commonService.showToasterError(res.message)
      }
     })
  }

  resendVerification(){
    this.isspinner = true;
    var data={
      "email":this.email
    }
    this.authenticationService.resendVerification(data).subscribe((res:any)=>{
      if(res.success){
        this.isspinner = false;
        console.log(" resend message is"+JSON.stringify(res));
      }
          })
  }

}
