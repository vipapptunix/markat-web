import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
declare var goSell: any;
export interface DialogData {
  animal: string;
  name: string;
}
interface Food {
  value: string;
  viewValue: string;
}

import {
  SearchCountryField,
  TooltipLabel,
  CountryISO
} from "ngx-intl-tel-input";
import { NotificationService } from '../services/notification.service';
import { MatSnackBar } from '@angular/material';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent {
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];
  counteryiso:string="CountryISO.India";
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom
  ];
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  SentData:string;
  data : any;
  countryDataArray;
  viewIs;
  country:any;
  countryCodeData=CountryISO.India;
  submitted:boolean = false;
  imgUrl:any;
  public cartdata:any;
  address:any;
  taxCharge:any;
  addresszero:any;
  maxPurchase:boolean = false;
  animal: string;
  shippingCharge:any;
  name: string;
  productList:any;
  amount:any;
  checkstate:boolean = false;
  i:any;
  addressId:any;
  productId:any;
  setid:any;
  mode:any;
  close:any;
  checkdata:any;
  address11:any;
  paymentmode:any = 'COD';
  settingpay:any;
  countcart:any;
  price_total:any;
  y:any;
  isshow:boolean = false;
  pricedata:any;
  addaddressform:FormGroup;
  countryList: any;
  prod_id: any;
  promocodeapp:string;
  language: any;
  subprice_total: any;
  promoDiscount: any;
  paymentid: any;
  lat: any;
  lng: any;
  state: any;
  @ViewChild("placesRef", { static: true }) placesRef: GooglePlaceDirective;
  mywindow: Window;
  currency: any;
  promo: any;
  checkstats: any;
  constructor(public dialog: MatDialog, private snackmsg:MatSnackBar, private router: Router,private translate: TranslateService,public authservice:AuthenticationService,private commonService: CommonService, private formBuilder: FormBuilder,private notify:NotificationService) 
  {
    this.addaddressform  = this.formBuilder.group({
      apartment: ['',Validators.required],
      buildingName  : ['',Validators.required],
      state: "punjab",
      city: ['',Validators.required],
      postalCode: [''],
      name: ['',Validators.required],
      phone: ['',Validators.required],
      country:['',Validators.required],
      countryCode:[''],
      addressType: "Home",
      _id:['']
    }); 
    this.authservice.languagechange.subscribe((res:any)=>
    {
      this.language = res;
    })

    this.authservice.getUpdatedsocial().subscribe((res:any)=>
    {
      this.settingpay = res.data[0].paymentMethods
    
    })
   }

   radioChapay(event)
   {
     
    this.paymentid = event.value;
   }

  openDialog(): void 
  {
  if(this.addresszero.length > 0)
  {
  if(this.paymentmode == 'CC' )
  {
    if(this.paymentid != undefined)
    {
      this.snackmsg.open("Loading for Payment",'Wait')
      const value ={
        "addressId": this.addressId,
        "paymentMethod":this.paymentmode,
        "paymentMethodId":this.paymentid,
        "promoCode":this.promocodeapp
        //"redirectUrl":'https://appgrowthcompany.com/mokabookweb/#/'
        }
        this.authservice.placedorder(value).subscribe((res:any)=>
        {
          if(res.status)
          {
            this.snackmsg.dismiss()
           this.mywindow = window.open(res.paymentURL,'', 'width=800,height=800,left=500,top=200')
            var userid =localStorage.getItem('userid')
            this.notify.checkListen().subscribe((res:any)=>
            {
              if(res.success)
              {
                localStorage.removeItem('cartcount')
              this.mywindow.close();
                if(res.paymentStatus == "SUCCSS") this.router.navigate(['/success']);
                if(res.paymentStatus == "FAILED") this.router.navigate(['/failure']);

              }
            })

          }
        }) 
    }
    else
    {
      this.snackmsg.open(this.language == 'en' ? "Please select method" : 'الرجاء تحديد الطريقة','',{
        duration:3000
      })
    }
  }

  if(this.paymentmode == 'COD' )
  {
    const value ={
    "addressId": this.addressId,
    "paymentMethod":this.paymentmode,
    "promoCode":this.promocodeapp
    //"redirectUrl":'https://appgrowthcompany.com/mokabookweb/#/'
    }
    this.authservice.placedorder(value).subscribe((res:any)=>
    {
      if(res.status)
      {
        localStorage.removeItem('cartcount');
        this.commonService.showToasterSuccess(this.language == 'en' ? "Order placed sucessfully!" : 'تم وضع الطلب بنجاح!')
        const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
          width: '250px',
          data: { name: this.name, animal: this.animal }
        });
    
        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          this.router.navigate(['/home']);
          this.animal = result;
        });
 
      }
    }) 
  }
  }
  else{
    this.snackmsg.open(this.language == 'en' ? 'Please Add address' : 'الرجاء إضافة العنوان',this.language == 'en' ? 'Thanks' : 'شكر') 
  }
}

  public AddressChange(address: any) {
    //setting address from API to local variable
    console.log(address);
    this.lat = address.geometry.location.lat()
    this.lng = address.geometry.location.lng()
    let length = address.address_components.length
    this.country = address.address_components[0].long_name;
    this.state = address.address_components[2].long_name;
    this.addaddressform.controls['state'].setValue(this.state)
    this.addaddressform.controls['city'].setValue(this.country)
  }
  
  checkpromocode(event)
  { 
    this.promocodeapp = event
    const data =
    {
      "promoCode":this.promocodeapp
    }
  
   this.authservice.promoCode(data).subscribe((res:any)=>
   {
     if(res.success)
     {
       console.log(res.finalCart)
       this.snackmsg.open(res.data.finalCart.promoMessage,'',{duration:3000})
       this.cartdata = res.data.cartUser;
       //this.amount = res.data[0].amount * res.data[0].quantity;
        this.productId = res.data.cartUser[0].product._id;
       this.shippingCharge = res.data.finalCart.shippingCharges
       this.taxCharge = res.data.finalCart.taxPercentage
       this.subprice_total = res.data.finalCart.preTaxAmount;
       this.price_total = res.data.finalCart.finalAmount
       this.promoDiscount = res.data.finalCart.promoDiscount
     }
   })
  }
  
  ngOnInit()
  {
    this.language = localStorage.getItem('lan')
    this.imgUrl = this.authservice.imageUrl;
  //   this.authservice.getcartlist().subscribe((res:any)=>{
  //    this.cartdata = res.data;
  //    this.productId = res.data[0].product._id;
  //  })

    this.authservice.getaddress().subscribe((data: any) => {
    this.address = data.data;
    this.addresszero  = data.data.filter(i=>i.isDefault == true);   
    this.addressId = this.addresszero[0]._id
  })


  this.authservice.getcartlist().subscribe((res:any)=>{

    
    this.getcartcount(res.data.length);
  })

  this.getCartlist();
  this.getPromocode()
  }
  public errorHandlingAddress = (control: string, error: string) => {
    return this.addaddressform.controls[control].hasError(error);
  }

  getPromocode()
  {
    this.authservice.getPromo().subscribe((res:any)=>
    {
      this.promo =res.data
    })
  }


  getCartlist()
  {
    this.authservice.Viewcartlist().subscribe((res:any)=>
    {
      this.currency = res.data.cartUser[0].product.currency
      this.cartdata = res.data.cartUser;
      //this.amount = res.data[0].amount * res.data[0].quantity;
       this.productId = res.data.cartUser[0].product._id;
      this.shippingCharge = res.data.finalCart.shippingCharges
      this.taxCharge = res.data.finalCart.taxPercentage
      this.subprice_total = res.data.finalCart.preTaxAmount;
      this.price_total = res.data.finalCart.finalAmount
      this.promoDiscount = res.data.finalCart.promoDiscount
    })
  
  }

  getAllcountry()
  {
    const data =
    {
      'search':this.addaddressform.controls['country'].value
    }
    this.authservice.getallCountry(data).subscribe((res:any)=>
    {
      this.countryList = res.data;
    })
  }
  goTohome()
  {
    this.router.navigate(['/home']);
  }

  radioclick(id,e)
  {
    const data ={ "id":id}
   this.authservice.defaultaddress(data).subscribe((data: any) => {
     if(data.success)
     {
      this.snackmsg.open(this.language == 'en' ? 'Changed Sucessfully!' : 'تغيرت بنجاح!',this.language == 'en' ? 'Address' : 'عنوان',
      {
        duration:3000
      })
      this.authservice.getaddress().subscribe((data: any) => {
        this.address = data.data;
        this.addresszero  = data.data.filter(i=>i.isDefault == true);
        this.addressId = this.addresszero[0]._id
      })
     }
  })
  }

  productSelected(i)
  {
   console.log("productSelect",i);
  }

 //input search value
 onKeyInProduct(e)
 {
   this.productList = this.countryList.filter(name=>name.name == e)
   console.log(this.countryList)
   console.log(this.productList)
 }

  deleteaddress(id)
  {
    this.setid = null;
    this.authservice.deleteaddress(id).subscribe((res:any)=>{
      if(res.success)
      {
        this.authservice.getaddress().subscribe((data: any) => {
        this.address = data.data;
        this.y = null;
        this.addresszero = [];
        this.addressId = null;
        console.log("adresszero",this.addresszero);
        })  
      }
    })
  }


  gotohome()
  {
    this.router.navigate(['/home']);
  }
  removecart(id)
  {
    this.authservice.removecarddata(id).subscribe((data:any)=>{
      if(data.status)
      {
        if(this.checkstate)
        {
          this.authservice.getcartlist().subscribe((res:any)=>{
            this.cartdata = res.data;
            //this.addresszero = res.data;
            this.getcartcount(res.data.length);
            if(res.data.length == 0)
            {
              this.router.navigate(['/home']);
            }
          });
        }
        else
        {
          this.authservice.getcartlist().subscribe((res:any)=>{
            this.cartdata = res.data;
            this.getcartcount(res.data.length);
            if(res.data.length == 0)
            {
              this.router.navigate(['/home']);
            }
          });
        }
       
      }
    });
  }


  subquantity(cartdata,id)
  {
   this.i = cartdata.quantity - 1;
   if(this.i != 0)
   {
     this.authservice.updatelist(cartdata,this.i,cartdata.product.price).subscribe((res:any)=>
     {
       if(res.status)
       {
         this.getCartlist()
       }
     })
   }
  
  }

  addquantity(cartdata1,id)
  {
    this.checkstats = this.cartdata.filter(i => i._id == id)
    this.i = this.checkstats[0].quantity + 1;
    this.prod_id =this.checkstats[0].product._id;

    if(this.i <= this.checkstats[0].product.purchaseQuantity)
    {
     this.authservice.updatelist(cartdata1,this.i,cartdata1.product.price).subscribe((res:any)=>
     {
       if(res.status)
       {
         this.getCartlist()
       }
     })
    }
   
    
  }

  radioChange(e)
  {
    this.isshow = true;
    if(e.value == 'COD')this.isshow = false;
    //if(e.value == 'CC') this.isshow = true; 
    this.paymentmode = e.value;
  }

  getcartcount(count)
  {
    this.countcart = 0;
    this.price_total = 0;
    for(var i=0;i<count;i++)
    {
        this.countcart =  this.cartdata[i].quantity + this.countcart;
       // this.price_total = ((this.cartdata[i].product.price * this.cartdata[i].quantity)-(this.cartdata[i].product.price * this.cartdata[i].quantity * (this.cartdata[i].product.discount)/100)) + this.price_total; 
    }
    this.authservice.getcartlength(this.countcart);

  }

  editaddressdata(id)
  {
    this.submitted = true;
    this.setid = id;
    this.authservice.getaddress().subscribe((data: any) => {
      this.address11 = data.data;
    })
    this.address11 = this.address.filter(r => r._id == id);
    this.addaddressform.patchValue(this.address11[0]);
  
  }

  addaddress(value) {
    
    this.submitted = true;
   this.country = this.addaddressform.controls['country'].value
   this.countryList = this.countryList.filter(ele=>ele.name == this.country)
   if(this.countryList.length == 0)this.commonService.showToasterError(this.language == 'en' ? "Please enter correct country name!" : 'الرجاء إدخال اسم الدولة الصحيح!')
   this.addaddressform.controls['countryCode'].setValue('+91')
   this.addaddressform.controls['country'].setValue(this.countryList[0].name)

   if(this.addaddressform.valid)
   {
  
     if(value == 2)
     {
       this.authservice.editaddress(this.addaddressform.value).subscribe((data:any)=>{
         if(data.success)
         {
           const val = 
          {
            "address":data.data._id
          }
          this.submitted = false;
           this.commonService.showToasterSuccess(this.language == 'en' ? "Address updated successfully!" : 'تم تحديث العنوان بنجاح!');
            this.authservice.getshippingCharge(val).subscribe((res:any)=>
            {
               this.shippingCharge = res.data;

            })
          this.authservice.getaddress().subscribe((data: any) => {
            this.address = data.data;
           // this.addresszero = this.address;
          //  this.dialog.close(DialogOverviewExampleDialog);
            this.addaddressform.reset();
          });
          }
       });
     }
      if(value == 1)
      {
        this.setid = null;
        if(this.addaddressform.valid)
        {
       
         this.authservice.addaddress(this.addaddressform.value).subscribe((res:any)=>{
            if(res.success)
            {
              const data= 
              {
                "address":res.data._id
              }
              this.submitted = false;
              this.commonService.showToasterSuccess(this.language == 'en' ? "Address Added successfully!" : 'تمت إضافة العنوان بنجاح!')
              this.authservice.getshippingCharge(data).subscribe((res:any)=>
              {
                
              })
             this.authservice.getaddress().subscribe((data: any) => {
                this.address = data.data;
                this.addresszero = [];
                this.addressId = null;
              //   if(this.address.length == 1)
              //   {
              //  this.addresszero = this.address;
              //   }
               this.addaddressform.reset();
             }) 
            }
         });

        }
       
     
      }
   }
   else
   {
     if(this.addaddressform.controls['phone'].invalid)
     {       
      this.commonService.showToasterError(this.language == 'en' ? "Invalid phone number" : 'رقم الهاتف غير صحيح');
     }
    // if(this.addaddressform.controls['phone'].value === null)
    // {
    //   this.commonService.showToasterError("Invalid phone number");
    // }
   }
     }

     addnewaddress()
     {
      this.submitted = false;
       this.setid = null;
       this.addaddressform.reset();
     }

     checkphone(e)
     {
       if(e.charCode != 45)
       {
         return true;
       }
       else{
         return false;
       }
     }

     openPaymentPage()
     {
       goSell.config({
         containerID:"root",
         gateway:{
           publicKey:"pk_test_Vlk842B1EA7tDN5QbrfGjYzh",
           merchantId: null,
           language:"en",
           contactInfo:true,
           supportedCurrencies:"all",
           supportedPaymentMethods: "all",
           saveCardOption:false,
           customerCards: true,
           notifications:'standard',
           callback:(response) => {
               console.log('response', response);
           },
           onClose: () => {
               console.log("onClose Event");
           },
           backgroundImg: {
             url: 'imgURL',
             opacity: '0.5'
           },
           labels:{
               cardNumber:"Card Number",
               expirationDate:"MM/YY",
               cvv:"CVV",
               cardHolder:"Name on Card",
               actionButton:"Pay"
           },
           style: {
               base: {
                 color: '#535353',
                 lineHeight: '18px',
                 fontFamily: 'sans-serif',
                 fontSmoothing: 'antialiased',
                 fontSize: '16px',
                 '::placeholder': {
                   color: 'rgba(0, 0, 0, 0.26)',
                   fontSize:'15px'
                 }
               },
               invalid: {
                 color: 'red',
                 iconColor: '#fa755a '
               }
           }
         },
         customer:{
           id:null,
           first_name: "First Name",
           middle_name: "Middle Name",
           last_name: "Last Name",
           email: "demo@email.com",
           phone: 
           {
               country_code: "965",
               number: "99999999"
           }
         },
         order:
         {
           amount: 100,
           currency:"KWD",
           items:[{
             id:1,
             name:'item1',
             description: 'item1 desc',
             quantity: '1',
             amount_per_unit:'00.000',
             discount: {
               type: 'P',
               value: '10%'
             },
             total_amount: '000.000'
           },
           {
             id:2,
             name:'item2',
             description: 'item2 desc',
             quantity: '2',
             amount_per_unit:'00.000',
             discount: {
               type: 'P',
               value: '10%'
             },
             total_amount: '000.000'
           },
           {
             id:3,
             name:'item3',
             description: 'item3 desc',
             quantity: '1',
             amount_per_unit:'00.000',
             discount: {
               type: 'P',
               value: '10%'
             },
             total_amount: '000.000'
           }],
           shipping:null,
           taxes: null
         },
        transaction:{
          mode: 'charge',
          charge:{
             saveCard: false,
             threeDSecure: true,
             description: "Test Description",
             statement_descriptor: "Sample",
             reference:{
               transaction: "txn_0001",
               order: "ord_0001"
             },
             metadata:{},
             receipt:{
               email: false,
               sms: true
             },
             redirect: "http://localhost/redirect.html",
             post: null,
           }
        }
       });
     }
}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {
  language: string;

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>, private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {this.language = localStorage.getItem('lan') }

  onNoClick(): void {
    this.dialogRef.close();
    this.router.navigate(['/home']);
  }

  selectaddres(id)
  {
    alert(id);
  }


  alphabate(event)
  {
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 65 && charCode < 90) || (charCode > 97 && charCode < 122)) {
      return true;
    }
    return false;
  }

  //payment gateway//
 
}