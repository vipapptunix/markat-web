import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerpageComponent } from './bannerpage.component';

describe('BannerpageComponent', () => {
  let component: BannerpageComponent;
  let fixture: ComponentFixture<BannerpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
