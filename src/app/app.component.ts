import { Component } from '@angular/core';
import { AuthenticationService } from './services/auth.service';
import { NotificationService } from './services/notification.service';
declare var google: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'nunu';
  language: string;
  gender: string;
  classlist : string;
  checlatlng: string;
  
  constructor(private notify:NotificationService,   public authenticationService: AuthenticationService,)
  {

    // sessionStorage.setItem('latitude','30.7046486');
    // sessionStorage.setItem('longitude','76.71787259999999')
    this.gender = localStorage.getItem('gender') == null ? 'M' : localStorage.getItem('gender')  
    localStorage.setItem('gender',this.gender)
    
    this.language = localStorage.getItem('lan') == null ? 'arb' : localStorage.getItem('lan')
    localStorage.setItem('lan',this.language)
    this.authenticationService.languagechange.subscribe((res:any)=>
    {
      if(res != '')
      {
        this.language = res
        location.reload()
      }
      this.classlist = this.language == 'en' ? 'abc' : 'Arb_md'
   
    })

  }




}
