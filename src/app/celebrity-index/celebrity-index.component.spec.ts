import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CelebrityIndexComponent } from './celebrity-index.component';

describe('CelebrityIndexComponent', () => {
  let component: CelebrityIndexComponent;
  let fixture: ComponentFixture<CelebrityIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CelebrityIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CelebrityIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
