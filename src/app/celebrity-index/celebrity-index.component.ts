import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';

@Component({
  selector: 'app-celebrity-index',
  templateUrl: './celebrity-index.component.html',
  styleUrls: ['./celebrity-index.component.scss']
})
export class CelebrityIndexComponent implements OnInit {
  celebrity: any;
  isloader = true;
  isfeature: any;
  imgurl:any;
  array = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
  celebcount: any;
  feature: string;
  celebid: any;
  language: string;
  constructor(private Srvc:AuthenticationService,private router: Router, private route: ActivatedRoute) { 
    this.route.queryParams.subscribe(params => {
      this.isfeature = params['feature']
      this.celebid = params['id']
    });
  }

  ngOnInit() {
    this.language = localStorage.getItem('lan')
    
    this.imgurl=this.Srvc.imageUrl;
    this.celebrityData('',this.isfeature)
 
  }


  goTocelebrityprofile(item) {
    console.log("bestseller",item)
    this.router.navigate(['/celebrityprofile'],{queryParams:{'id':item._id}});
  }

celebrityData(item,value)
{
  if(this.feature == 'all')
  {
    const data=
    {
      "page":1,
      "count":100,
      "albhabet": item
    }
    this.Srvc.celebrityData(data).subscribe((res:any)=>
    {
      this.isloader = false;
       this.celebrity = res.data
       //this.celebcount = res.data.length
       
    })
  }
  if(this.feature != 'all')
  {
    const data=
    {
      "page":1,
      "count":100,
      "isFeatured":value,
      "albhabet": item
    }
    this.Srvc.celebrityData(data).subscribe((res:any)=>
    {
      this.isloader = false;
       this.celebrity = res.data
       //this.celebcount = res.data.length
       
    }) 
  }

}

getAllcelebrity(item)
{
  this.isloader = true;
 if(item == '')
 {
   this.feature = 'all'
   this.celebrityData(item,'') 
 }
   if(item != '')this.celebrityData(item,this.isfeature)
}
}
