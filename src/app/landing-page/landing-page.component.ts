import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  language: string;
  lng: any;
  lat: any;

  constructor( private router: Router,private _route :ActivatedRoute) {this.language = localStorage.getItem('lan') == null ? 'arb' : localStorage.getItem('lan')}

  ngOnInit() {
    if(navigator)
    {
      navigator.geolocation.getCurrentPosition(pos => {
        this.lng = +pos.coords.longitude;
        this.lat = +pos.coords.latitude;
        sessionStorage.setItem('latitude',this.lat);
        sessionStorage.setItem('longitude',this.lng);
      });
    }
    else
    {
     sessionStorage.setItem('latitude','30.7046486');
     sessionStorage.setItem('longitude','76.71787259999999')
    }
 
    // console.log("landing pages");
    // sessionStorage.setItem('latitude','30.7046486');
    // sessionStorage.setItem('longitude','76.71787259999999')
  }

  gotohome(item){
    localStorage.setItem('gender',item)
    this.router.navigate(['/home'],{queryParams:{item}})

  }

}
