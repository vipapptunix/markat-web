import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-subcategeory',
  templateUrl: './subcategeory.component.html',
  styleUrls: ['./subcategeory.component.scss']
})
export class SubcategeoryComponent implements OnInit {

  index = 0
  id: any = "";
  subCategeory: any = "";
  currentPage: number = 1;
  search_result: any;
  searchvalue: any;
  imgurl: any;
  subproduct: boolean = false;
  searchlist: boolean = false;
  categorylist: any;
  bannerlist: any;
  searchresulthide: boolean = false;
  listsubcategory: any;
  subcategeorylist: any;
  categorycartlist: any;
  categoryid: any;
  productdetail: any;
  fixedrate: any;
  length: any;
  realid: any;
  pagehide: boolean = false;
  realcategory: any;
  row: boolean = true;
  pricehide:boolean = true;

  value: number = 40;
  highValue: number = 6000;
  options: Options = {
    floor: 0,
    ceil: 5000
  };
  maxvalue: any;
  bannerlist_id: any;
  bannerlist_type: any;
  bannerlist_list: any;
  constructor(private router: Router, private route: ActivatedRoute, public authenticationService: AuthenticationService) {
    this.imgurl = this.authenticationService.imageUrl;

    // this.authenticationService.getDashboard().subscribe((res: any) => {
    //   this.categorylist = res.data.output[1].value;

    // })

    this.authenticationService.$checkproductid.subscribe((res: any) => {
    
     this.headerproductcategory(res)

    });
  }

  ngOnInit() {


    this.authenticationService.$searchvalue.subscribe((res: any) => {
      this.searchvalue = res;
    
    })

    // this.getsearchdata(this.searchvalue);
    this.authenticationService.$bannerlist.subscribe((data:any)=>
    {
      this.getbannnerdatalist(data)
      // if(data.length != 0)
      // {
      //   this.authenticationService.getbannerproduct(data._id,data.offer.list).subscribe((res:any)=>
      //   {

      //     if(res.total == 0)
      //     {
      //       this.router.navigate(['banner'])
      //     }
      //   });
      // }
     
    })

  }


  sliderEvent(page,size)
  {

    console.log("check",this.value);
    // const data = {
    //   "minPrice":this.value,
    //   "maxPrice":this.highValue
    // }
 
    const bandata =
    {
      "count": size ? size: '10',
      "page": page ? page : '1',
      "id": this.bannerlist_id,
      "list": this.bannerlist_list,
      "type": this.bannerlist_type,
      "sortBy": 1,
      "minPrice": this.value,
      "maxPrice": this.highValue
    }

   this.authenticationService.getbannerprolist(bandata).subscribe((res)=>{
    this.length = res.total;
    this.search_result = res.data;
    // this.maxvalue = from(this.search_result).pipe(max(this.search_result.price)).subscribe(res=>{
    //   console.log(this.maxvalue)
    // });
    this.pagehide = false;
    if (res.status) {
      this.pricehide = true;
      this.productdetail = res.data;
      //this.fixedrate = this.productdetail.price * this.productdetail.discount / 100;
      this.subproduct = true;
      this.searchlist = false;
      this.searchresulthide = true;
    }
   })
  }
  getsearchdata(value)
   {
    const data =
    {
      "limit": 100,
      "skip": 0,
      "searchText": value,
      "userId": localStorage.getItem('userid')
    }
    this.authenticationService.searchproduct(data).subscribe((res: any) => {
      this.searchresulthide = false;
      this.length = res.total;
      this.search_result = res.data;
      this.searchlist = false;
      this.subproduct = false;
      if (this.search_result.length == 0) {
        this.router.navigate(['/notfound']);
      }
 
    });
  }



  getcategoryid(catid, subid, i) {
    this.index = i
    if (subid == 1) {
      this.searchlist = true;
      this.searchresulthide = true;
      this.subproduct = false;
      this.categoryid = catid;
      this.authenticationService.getcategoryTablist(catid).subscribe((res: any) => {
        this.pagehide = true;
        this.pricehide = false;
        this.bannerlist = res.data[1].value;
        this.subcategeorylist = res.data[2].value;
        this.listsubcategory = res.data[3].value;
        console.log("catpro",res.data);
        // this.categorylist = this.categorylist[0].filter(id=>id.label == 'Categories')

      })
    }
    else {
      this.realid = catid;
      this.realcategory = this.categoryid;
      const data =
      {
        "id": catid,
        "page": 1,
        "count": 10,
        "category": this.categoryid,
        "isRecommended": false,
        "isFeatured": false,
        "minPrice": this.value,
        "maxPrice": this.highValue,
        "discount": [
            10,
        ],
        "categories": [
            "5eb9151da49e9d6d7a988869"
        ],
        "subCategories": [
            "5f0408bf01e0501ddef617fd"
        ]
      }

      this.authenticationService.getprouctdata(data).subscribe((res: any) => {
        console.log("yyyyyy",res);

        this.length = res.total;
        this.search_result = res.data;
        // this.maxvalue = from(this.search_result).pipe(max(this.search_result.price)).subscribe(res=>{
        //   console.log(this.maxvalue)
        // });
        this.pagehide = false;
        if (res.status) {
          this.pricehide = true;
          this.productdetail = res.data;
          //this.fixedrate = this.productdetail.price * this.productdetail.discount / 100;
          this.subproduct = true;
          this.searchlist = false;
          this.searchresulthide = true;
        }
      })
    }

  }

  gotoproductDetails(id) 
  {
    this.router.navigate(['productdetail'], { queryParams: { 'id': id } });
  }


  changesearchresult(value) 
  {
    if (value.length == 0) {
      this.router.navigate(['/home'])
    }
    else {
      this.getsearchdata(value);
    }
    //this.authenticationService.searchdata(this.searchvalue);
  }

  getbannnerdatalist(i)
  {
  
  console.log(i)
    this.bannerlist_id = i._id
    this.bannerlist_list = i.offer.list
    this.bannerlist_type = i.offer.type
    const data =
    {
      "count": "10",
      "page": "1",
      "id": this.bannerlist_id,
      "list": this.bannerlist_list,
      "type": this.bannerlist_type,
      "sortBy": 1,
      "minPrice": 0,
      "maxPrice": 5000
    }

   this.authenticationService.getbannerprolist(data).subscribe((res)=>{
    this.length = res.total;
    this.search_result = res.data;
    // this.maxvalue = from(this.search_result).pipe(max(this.search_result.price)).subscribe(res=>{
    //    console.log(this.search_result)
    //  });
    // for(let i = 0;i<res.data.length;i++)
    // {
       
    //   temp.push(res.data[i].price);
    // }
  
    // this.maxvalue = from(temp).pipe(max()).subscribe((res)=>
    // {
    //  this.options.ceil= res;
    //   // this.options.floor = 0;
    // })
    this.pagehide = false;
    if (res.status) {
      this.pricehide = true;
      this.productdetail = res.data;
      //this.fixedrate = this.productdetail.price * this.productdetail.discount / 100;
      this.subproduct = true;
      this.searchlist = false;
      this.searchresulthide = true;
    }
   })

  }



  headerproductcategory(res) 
  {
    this.realid = res.id;
    this.realcategory = res.catid;
    const data =
    {
      "id": res.id,
      "page": 1,
      "count": 10,
      "category": res.catid,
      "isRecommended": false,
      "isFeatured": false
    }
    this.authenticationService.getprouctdata(data).subscribe((res: any) => {
       this.search_result = res.data;
       this.length = res.total;
      if (res.status) {
         this.productdetail = res.data;
         this.pricehide = true;
         //this.fixedrate = this.productdetail.price * this.productdetail.discount / 100;
         this.subproduct = true;
         this.searchlist = false;
         this.searchresulthide = true;
      }
    })
  }

  backtohome()
  {
    this.router.navigate(['/home']);
  }

  pageeventvalue(e,value) 
  {
    this.sliderEvent(e.pageIndex+1,e.pageSize)
    // const data =
    // {
    //   "id": this.realid,
    //   "page": e.pageIndex + 1,
    //   "count": e.pageSize,
    //   "category": this.categoryid,
    //   "isRecommended": false,
    //   "isFeatured": false
    // }
    // if (e.pageIndex >= 0 || e.pageSize >= 5) {
    //   this.authenticationService.getprouctdata(data).subscribe((res: any) => {
    //     this.pricehide = true;
    //     this.search_result = res.data;
    //     if (res.status) {
    //       this.productdetail = res.data;
    //       //this.fixedrate = this.productdetail.price * this.productdetail.discount / 100;
    //       this.subproduct = true;
    //       this.searchlist = false;
    //       this.searchresulthide = true;
    //     }
    //   })
    // }

  }

  rowmethod(e)
  {
    if(e == 1)
    {
      this.row = false;
    }
    else{
      this.row = true;
    }
  }
}
