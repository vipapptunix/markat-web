import { Component, OnInit, HostListener } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Console } from 'console';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { AuthenticationService } from '../services/auth.service';
import { NgxSpinnerService } from "ngx-spinner";
import { NgxImgZoomService } from "ngx-img-zoom";
import { CommonService } from '../services/common.service';
declare const $: any;
@Component({
  selector: 'app-productdetail',
  templateUrl: './productdetail.component.html',
  styleUrls: ['./productdetail.component.scss']
})
export class ProductdetailComponent implements OnInit {
  id: any;
  imgurl: any;
  Isloader:boolean = true;
  enableZoom:any;
  submitted:boolean = false
  productDetails: any;

  language: any;
  specificationsDetail: any[] = [];
  imageshow: any;
  name: string;
  cartname: any;
  comparearray = [];
  favproduct: any;
  GuestData:any;
  homeData: any;
  filterfav: any;
  description: any;
  Ratiingdata: any;
  isloggin: any;
  cartlist: any;
  highlights: any;
  proid: any;
  bestsellerData: any;
  element: any;
  guestArray:any[] = [];
  productImages: any[] = [];
  imageSelected: any;
  bestsellerDataVal: any[] = [];
  productdetails:any;
  // shareObj = {
  //   href: `https://apptunix.page.link/share/product_id='${this.id}`,
  //   hashtag: "#FACEBOOK-SHARE-HASGTAG"
  // };
  namelan: any;
  specificationsDetaillan: any;
  descriptionlan: any;
  highlightslan: any;
  lang: any;
  userid: string;
  brand: any;
  brandlist: any;
  celebdata: any;
  pincode: any = '';
  count: number= 0;
  href: string;
  currency: any;
  customRecommended2: { rtl: boolean; loop: boolean; mouseDrag: boolean; freeDrag: boolean; autoplay: boolean; touchDrag: boolean; pullDrag: boolean; dots: boolean; dotsEach: boolean; nav: boolean; slideBy: number; margin: number; autoWidth: boolean; navText: string[]; responsive: {}; };
  customRecommended: { rtl: boolean; loop: boolean; mouseDrag: boolean; freeDrag: boolean; autoplay: boolean; touchDrag: boolean; pullDrag: boolean; dots: boolean; dotsEach: boolean; nav: boolean; autoWidth: boolean; navText: string[]; responsive: {}; };
  name_ar: any;
  brand_ar: any;

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private authenticationService: AuthenticationService,
    private commonService: CommonService,
    private translate:TranslateService,
    private snackmsg:MatSnackBar,
    private ngxImgZoom: NgxImgZoomService,
    private _route: ActivatedRoute) { 
      this.ngxImgZoom.setZoomBreakPoints([
        { w: 100, h: 100 },
        { w: 150, h: 150 },
        { w: 200, h: 200 },
        { w: 250, h: 250 },
        { w: 300, h: 300 }
      ]); 
      // this._route.queryParams.subscribe(params => {this.language = params['lan']});
      this.href = `https://apptunix.page.link/share/product_id='${this.id}`,
      this.authenticationService.languagechange.subscribe((res:any)=>
      {
        this.language = res;
      })
      this._route.queryParams.subscribe(params => { this.id = params['id'] });
  this.spinner.show()
   
    }

  ngOnInit() {
    
    this.userid = localStorage.getItem('userid')
    this.isloggin = localStorage.getItem("sucess");
    this.imgurl = this.authenticationService.imageUrl;
    this.language = localStorage.getItem('lan')
    this.translate.setDefaultLang(this.language);
    this.comparearray.push(this.id)
    this.getproductdetails(this.id);
    //this.getDashboard()
    this.getAllrating()
    
    this.authenticationService.$updatepoduct.subscribe((res)=>
    {
      // this._route.queryParams.subscribe(params => { this.id = params['relatedproduct'] });
      // if(this.id == 'related&product')
      // this.getproductdetails(res);
    })

    this.authenticationService.$coutChng.subscribe((res:any)=>
    {
      if(res == 'curr')
      {
        this.getproductdetails(this.id);
      }
    })

    this.customRecommended = {
      rtl:this.language == 'arb' ? true : false,
      loop: false,
      mouseDrag: false,
      freeDrag:true,
      autoplay:false,
      touchDrag: false,
      pullDrag: false,
      dots: false,
      dotsEach:false,
      nav: true,
      autoWidth:false,
  
      navText: ['<<', '>>'],
      responsive: {
        0: {
          items: 2
        },
        400: {
          items: 4
        },
        740: {
          items: 4
        },
        940: {
          items: 4
        }
      },
     
    }
  
    this.customRecommended2 = {
      rtl:this.language == 'arb' ? true : false,
      loop: false,
      mouseDrag: false,
      freeDrag:true,
      autoplay:false,
      touchDrag: false,
      pullDrag: false,
      dots: false,
      dotsEach:false,
      nav: true,
      slideBy: 5,
      margin: 20,
      autoWidth:false,
      navText: ['<<', '>>'],
      responsive: {
        0: {
          items: 2
        },
        400: {
          items: 4
        },
        740: {
          items: 4
        },
        940: {
          items: 5
        }
      },
     
    }




  }

  gotocompare() {
    sessionStorage.setItem('compareid',JSON.stringify(this.comparearray))
    this.router.navigate(['compare'])
  }

  getproductdetails(id) {

   const data={
     "product":id,
     "userId":this.userid,
     "search":false
   }
    this.authenticationService.productDetail(data).subscribe((res: any) => {
    
      this.cartlist = res.data;
      this.cartname = res.data.name;
      this.productdetails = res.relatedProducts
      console.log('product',res)
      this.currency = res.data.currency;
      //this.currency = res.data.
      if (res.success) {
        this.spinner.hide()
        this.submitted = true;
        this.Isloader = false;
        this.productDetails = res.data;
        this.bestsellerDataVal = this.productDetails.isFavourite
        this.productImages = this.productDetails.images;
        if (this.productImages !== null && this.productImages !== undefined && this.productImages.length > 0) {
          this.imageSelected = this.productImages[0];
        }

          this.brandlist = res.brands;
          this.celebdata = res.celebrityEndorsed
          this.specificationsDetail = this.productDetails.specifications;
          this.description = this.productDetails.description;
          this.descriptionlan = this.productDetails.description_ar;
          this.highlightslan = this.productDetails.highlights_ar;
          this.name = this.productDetails.name;
          this.name_ar = this.productDetails.name_ar;
          this.brand_ar = this.productDetails.brand.name_ar;
          this.brand = this.productDetails.brand.name
          this.highlights = this.productDetails.highlights;
  
      }
    })
  }

  changeImage(id) {
    this.imageSelected = this.productImages.filter(ij => ij._id == id)[0];
    $(this).css('border', 'solid #000000')
  }


  goTocelebrityprofile(item) {
    this.router.navigate(['/celebrityprofile'],{queryParams:{'id':item}});
  }

  goTocart(cart) {
    console.log('cart')
    if (!this.isloggin) {
    
      const data =
          {
            "productId": this.cartlist._id,
            "amount": cart.price - (cart.price*cart.discount)/100,
            //"amount": (this.productDetails.price - ((this.productDetails.price*this.productDetails.discount)/100)),
            "quantity": 1
          }
        if(localStorage.getItem('cartcount'))
        {
          this.guestArray = JSON.parse(localStorage.getItem('cartcount'))
          if(this.guestArray)
          {
            var sum = 0;
            this.guestArray.forEach(element => {
              if(element.productId == cart._id)
              {
                element.quantity = element.quantity + 1;
              
              }
          
            });
            if(this.guestArray.filter(ele => ele.productId == cart._id).length < 1)
            {
              this.guestArray.push(data)
            }
          }
          else
          {
              this.guestArray.push(data);
          }
          
          
        }else
        {
          this.guestArray.push(data)
        }
      this.router.navigate(['/cart']);

      console.log("GuestDAta",this.GuestData)
     
      localStorage.setItem('cartcount',JSON.stringify(this.guestArray))
    }
    else {
      const pstdata =
      {
        "data": [
          {
            "product": this.cartlist._id,
            //"amount": this.cartlist.price,
            //"amount": (this.productDetails.price - ((this.productDetails.price*this.productDetails.discount)/100)),
            "quantity": 1
          }
        ]
      }
      this.authenticationService.postcartlist(pstdata).subscribe((res: any) => {

        if (res.status) {
          
          this.router.navigate(['/cart']);
        }
        else{
          this.snackmsg.open(res.message,'Thanks',{duration:1000})
        }
      });
    }


  }

  gotoproductDetails(id){
  window.open(`http://appgrowthcompany.com/markat_web/#/productdetail?id=${id}`,'_blank')
  }

  gotoproductDetailsB(id)
  {
    this.router.navigate(['/categories'],{queryParams:{"id":id,"type":'brand'}})
  }

  filterBrancd(e,id)
  {
   
    if(e.checked)
    { 
      if(this.comparearray.length < 4)
      {
        this.comparearray.push(id)
        this.count = this.comparearray.length
        console.log(this.comparearray)
      }
      else{
        this.snackmsg.open(this.language == 'en' ? 'Maximum Four item can be compared' : 'يمكن مقارنة أربعة عناصر كحد أقصى',this.language == 'en' ? 'Thanks' : 'شكر',{duration:3000})
      }
    }
    if(!e.checked)
    {
      this.comparearray = this.comparearray.filter(brand => brand != id)
      this.count = this.comparearray.length
      console.log(this.comparearray)
    }
  }
  Addtobuy() {
    const pstdata =
    {
      "data": [
        {
          "product": this.cartlist._id,
          "quantity": 1
        }
      ]
    }
    if (this.isloggin) {
      this.authenticationService.postcartlist(pstdata).subscribe((res: any) => {

        if (res.status) {
          this.router.navigate(['/checkout']);
        }
        else{
          
            this.snackmsg.open(res.message,'Thanks',{duration:1000})
          
        }
      });
    }
    else
    {
      this.snackmsg.dismiss();
      this.snackmsg.open(this.language == 'en' ? 'Please login !' : 'الرجاء تسجيل الدخول !',this.language == 'en' ? 'Login' : 'تسجيل الدخول',{
        duration: 4000,
      }).onAction().subscribe((res)=>
      {
        this.router.navigate(['/login'])
      })
    }
  }

  goTocheckout() {
    this.router.navigate(['/checkout']);

  }
  goTologin() {
    this.router.navigate(['/login']);
  }
  goTosignup() {
    this.router.navigate(['/signup']);
  }


  addfavlist() {
    if (this.isloggin != null) {
      this.authenticationService.postfavlist(this.proid).subscribe((res: any) => {
        if (res.success) {
         // this.getDashboard()
        }

      });
    }

  }



  getAllrating() {
    const data =
    {
      'product': this.id,
    }
    this.authenticationService.getAllRating(data).subscribe((res: any) => {
      this.Ratiingdata = res.data
    })
  }

  moveto() {
    this.router.navigate(['/home']);
  }


  Help()
  {
    this.router.navigate(['/terms&cond'],{queryParams:{'name':'Help'}})
  }
  languageChange(lan)
  {
    this.translate.use(lan)
    localStorage.setItem('lan',lan)
    // window.location.reload()
    this.lang = lan
    if(this.lang == 'en') 
    if(this.lang == 'arb') 
    this.authenticationService.languageChange(lan)
   //this.router.navigate([],{queryParams:{lan}})
  }
  gotoWhatsapp()
  {
    window.open('https://api.whatsapp.com/send?phone=+918288932697')
  }

  checkproddetails()
  {
    
    if(this.pincode != '')
    {
      const data=
      {
        "pincode":this.pincode
      }
    this.authenticationService.checkdeliveryloc(data).subscribe((res:any)=>
    {
      if(res.data.isDelivery)
      {
        this.snackmsg.open(this.language == 'en' ? 'Available at this address' : 'متاح في هذا العنوان',this.language == 'en' ? 'Buy now':'اشتري الآن',{
          duration: 8000,
        })
      }
      else{
        this.snackmsg.open(this.language == 'en' ? 'Oops..' : 'وجه الفتاة..',this.language == 'en' ? 'Not availble at this address' : 'غير متوفر في هذا العنوان',{
          duration: 8000,
        })
      }
    })
    }

  }

  alphabate(event)
  {
   // alert(event.keyCode)
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 65 && charCode < 90) || (charCode > 97 && charCode < 122)) {
      return false;
    }
    return true;
  }
}
